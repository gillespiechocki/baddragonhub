<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'baddragonhub');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zDIC@OBf(E8hGloeuxGD)8gP|08[L%U{e%G_4=]2iX0!a~^D^;<H5 E#]wuSI,cL');
define('SECURE_AUTH_KEY',  '`D=@DG3z+X0R|B%^f7[&:00GhmvOj-z@sPacIc9=ScFGu_Bc6r%0fNrN1T%1(!u3');
define('LOGGED_IN_KEY',    '+~<J8)u,Q9T:-RmtV@R&BoZg6dYut>,HL{H+5Z#wW +jClh!P:RTT_b=tjE(WB~1');
define('NONCE_KEY',        '!02Pt9:IFThmCXYKrQ^J/ZQVY<EPjGUc-|~PL%g*o&_ONb<)bqAn$6}?zrC,1Oz_');
define('AUTH_SALT',        'U}@C.Q^-&-ZY@Glgtx-eMSZBgI9io%*.1~lW<Zw%VK0M)1X^5H}1!u:Ikt cnXVM');
define('SECURE_AUTH_SALT', 'R?7%-q 8f?/oJ?RhE?x^J,%6i-juWTLI*MD,W5fZkOSVS6bxpyzXh>,ee!Bq_>9[');
define('LOGGED_IN_SALT',   '+%D^:p{D~bg^IKI~ -M.L!S2p?_lgGJ4hEjTi4|3tbmv$c))g4%oH:U}lq`AmEuM');
define('NONCE_SALT',       '6rf1lr9qSMkWZ6^LiRKT^]507@(;tzdOO0 hRDYX[gB&Ua2U[qK^pgCNlf#HerAS');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');