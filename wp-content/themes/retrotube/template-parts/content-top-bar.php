<?php if(xbox_get_field_value( 'wpst-options', 'show-social-profiles' ) == 'on' || xbox_get_field_value( 'wpst-options', 'enable-membership' ) == 'on') : ?>

    <div class="top-bar">
        <div class="top-bar-content row">
            <div class="social-share">
            <?php if( xbox_get_field_value( 'wpst-options', 'show-social-profiles' ) == 'on' ) : ?>
                <small><?php esc_html_e('Follow me on:', 'wpst'); ?></small>
                <?php if(xbox_get_field_value( 'wpst-options', 'facebook-profile' ) != '') : ?>
                    <a href="<?php echo xbox_get_field_value( 'wpst-options', 'facebook-profile' ); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                <?php endif; ?>
                <?php if(xbox_get_field_value( 'wpst-options', 'twitter-profile' ) != '') : ?>
                    <a href="<?php echo xbox_get_field_value( 'wpst-options', 'twitter-profile' ); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                <?php endif; ?>
                <?php if(xbox_get_field_value( 'wpst-options', 'google-plus-profile' ) != '') : ?>
                    <a href="<?php echo xbox_get_field_value( 'wpst-options', 'google-plus-profile' ); ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
                <?php endif; ?>
                <?php if(xbox_get_field_value( 'wpst-options', 'youtube-profile' ) != '') : ?>
                    <a href="<?php echo xbox_get_field_value( 'wpst-options', 'youtube-profile' ); ?>" target="_blank"><i class="fa fa-youtube"></i></a>
                <?php endif; ?>
                <?php if(xbox_get_field_value( 'wpst-options', 'tumblr-profile' ) != '') : ?>
                    <a href="<?php echo xbox_get_field_value( 'wpst-options', 'tumblr-profile' ); ?>" target="_blank"><i class="fa fa-tumblr"></i></a>
                <?php endif; ?>
                <?php if(xbox_get_field_value( 'wpst-options', 'instagram-profile' ) != '') : ?>
                    <a href="<?php echo xbox_get_field_value( 'wpst-options', 'instagram-profile' ); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                <?php endif; ?>
            <?php endif; ?>
            </div>

            <?php if( xbox_get_field_value( 'wpst-options', 'enable-membership' ) == 'on' ) : ?>
                <div class="membership">   
                    <ul>                                     
                    <?php if( is_user_logged_in() ) : ?>
                        <li><span class="welcome"><i class="fa fa-user"></i> <?php esc_html_e('Welcome', 'wpst');?> <?php echo wp_get_current_user()->display_name; ?></span></li>
                        <?php if( xbox_get_field_value( 'wpst-options', 'display-video-submit-link' ) == 'on' ) : ?>
                            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>submit-a-video"><i class="fa fa-play-circle"></i> <?php esc_html_e('Submit a Video', 'wpst'); ?></a></li>
                            <li class="separator">|</li>
                        <?php endif; ?>  
                        <?php if( xbox_get_field_value( 'wpst-options', 'display-my-channel-link' ) == 'on' ) : ?>        
                            <li><a href="<?php echo get_author_posts_url(get_current_user_id()); ?>"><i class="fa fa-video-camera"></i> <?php esc_html_e('My Channel', 'wpst'); ?></a></li>
                            <li class="separator">|</li>
                        <?php endif; ?>
                        <?php if( xbox_get_field_value( 'wpst-options', 'display-my-profile-link' ) == 'on' ) : ?>
                            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>my-profile"><i class="fa fa-user"></i> <?php esc_html_e('My Profile', 'wpst'); ?></a></li>
                            <li class="separator">|</li>
                        <?php endif; ?>
                        <li><a href="<?php echo esc_url(wp_logout_url(is_home()? home_url() : get_permalink()) ); ?>"><i class="fa fa-power-off"></i> <?php esc_html_e('Logout', 'wpst'); ?></a></li>
                    <?php else : ?>
                        <li><span class="welcome"><i class="fa fa-user"></i> <?php esc_html_e('Welcome Guest', 'wpst');?></span></li>
                        <li class="login"><a href="#wpst-login"><?php esc_html_e('Login', 'wpst'); ?></a></li>
                        <li class="or"><?php esc_html_e('Or', 'wpst');?></li>
                        <li class="login"><a href="#wpst-register"><?php esc_html_e('Register', 'wpst'); ?></a></li>
                    <?php endif; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>

<?php endif; ?>