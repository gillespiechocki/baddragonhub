jQuery(document).ready(function(){

    toggleDisplayerOptions();

    function toggleDisplayerOptions(){
        jQuery('div[data-field-id]').each(function(e){
            var fieldId = jQuery(this).data('field-id');
            if( fieldId.indexOf('displayed-when') > -1 ){
                var data = fieldId.split(':');
                var displayer = {'type' : data[1], 'id' : data[2], 'value' : data[3]};
                if( displayer.type == 'switch' ){
                    if( jQuery('#' + displayer.id ).attr('value') == displayer.value ){
                        jQuery(this).slideDown( 'fast' );
                    }else{
                        jQuery(this).slideUp( 'fast' );
                    }
                }
                if( displayer.type == 'radio' ){
                    if( jQuery('div.checked').children('input[name="' + displayer.id + '"]').attr('value') == displayer.value ){
                        jQuery(this).slideDown( 'fast' );
                    }else{
                        jQuery(this).slideUp( 'fast' );
                    }
                }
            }

        });
    }

    jQuery('.xbox-sw-wrap').live('click', function(e){ 
        toggleDisplayerOptions();
    });

    jQuery('label').live('click', function(e){        
        toggleDisplayerOptions();
    });


    //ajax options save
    jQuery('.xbox-form').live('submit', function(e){
        e.preventDefault();

        var values = {};
        jQuery.each(jQuery('.xbox-form').serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });

        var object_id = jQuery(this).find('div').data('object-id');

        var button_text = jQuery('#xbox-save').html();

        jQuery('#xbox-save').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Saving Changes...');

        jQuery.ajax({
            method: "POST",
            url:    xbox_ajax_var.url,
            data       : {
                            action      : 'xbox_ajax_save_form',
                            nonce       : xbox_ajax_var.nonce,
                            object_id   : object_id, 
                            values      : values
            },
            dataType   : "json"
        }).done(function(data) {
            console.log(data);
            jQuery('#xbox-save').html('<i class="fa fa-check"></i> Changes Saved');
            setTimeout(function(){jQuery('#xbox-save').html(button_text);}, 1000);
        }).fail(function(jqXHR, textStatus) {
            console.error( "Request failed: " + textStatus );
            jQuery('#xbox-save').html('<i class="fa fa-times"></i> Changes Failed');
            setTimeout(function(){jQuery('#xbox-save').html(button_text);}, 1000);
        }).always(function() {
        });

        // jQuery.ajax({
        //     type       : "POST",
        //     url        : xbox_ajax_var.url,
        //     data       : {
        //                     action      : 'xbox_ajax_save_form',
        //                     nonce       : xbox_ajax_var.nonce,
        //                     object_id   : object_id, 
        //                     values      : values
        //     },
        //     dataType   : "json",
        //     beforeSend : function(){
        //         jQuery('#xbox-save').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Saving Changes...');
        //     },
        //     success    : function(data){
        //         console.log(data);
        //         jQuery('#xbox-save').html('<i class="fa fa-check"></i> Changes Saved');
        //         setTimeout(function(){jQuery('#xbox-save').html(button_text);}, 1000);
        //     },
        //     error     : function(jqXHR, textStatus, errorThrown) {
        //         console.error(errorThrown);
        //         jQuery('#xbox-save').html('<i class="fa fa-times"></i> Changes Failed');
        //         setTimeout(function(){jQuery('#xbox-save').html(button_text);}, 1000);
        //     }
        // });

    });

});