<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_logs_page() {

?>
<div id="wp-script">
    <div class="content-tabs">

        <div class="row">
            <div class="col-xs-12 padding-top-10 padding-bottom-10">
                <div class="logo">
                    <h2><img class="panda" src="<?php echo WPSCORE_URL; ?>admin/assets/images/panda.svg" width="40"/> <img class="text" src="<?php echo WPSCORE_URL; ?>admin/assets/images/logo.svg" width="200"/></h2>                                      
                </div>
            </div>
        </div>

        <?php WPSCORE()->display_tabs();?>

        <div class="tab-content">
            <div class="tab-pane active" id="logs" >

                <div v-cloak class="padding-top-15">


                    <div class="row text-center v-cloak--block">
                        <div class="col-xs-12"><p><i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Loading Core...</span></p></div>
                    </div>

                    <div class="v-cloak--hidden">

                        <!--**************-->
                        <!-- LOADING DATA -->
                        <!--**************-->
                        <template v-if="loading.loadingData">
                            <div class="row text-center">
                                <div class="col-xs-12"><p><i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Loading Data...</span></p></div>
                            </div>
                        </template>

                        <transition name="fade">
                            
                            <template v-if="dataLoaded">

                                <div v-if="logs.length == 0" class="row">
                                    <div class="col-xs-12"><p class="text-center"><?php esc_html_e('No log has been written yet', WPSCORE_LANG);?></p></div>
                                </div>

                                <div v-else class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-default btn-sm" @click.prevent="copyLogs"><i class="fa" :class="loading.copyLogs ? 'fa-spinner fa-pulse fa-fw' : 'fa-clipboard'" aria-hidden="true"></i> <?php esc_html_e('Copy Logs to clipboard', WPSCORE_LANG);?></button>
                                            <button class="btn btn-danger btn-sm" @click.prevent="deleteLogs"><i class="fa" :class="loading.deleteLogs ? 'fa-spinner fa-pulse fa-fw' : 'fa-trash-o'" aria-hidden="true"></i> <?php esc_html_e('Delete Logs', WPSCORE_LANG);?></button>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 margin-top-10">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tr>
                                                <th width="140"><?php esc_html_e('Date', WPSCORE_LANG);?></th>
                                                <th>
                                                    <form class="form-inline">
                                                    <?php esc_html_e('Type', WPSCORE_LANG);?>
                                                        <select name="logsType" class="form-control" v-model="filters.type">
                                                            <option value="">       <?php esc_html_e('All',     WPSCORE_LANG);?></option>
                                                            <option value="success"><?php esc_html_e('Success', WPSCORE_LANG);?></option>
                                                            <option value="notice"> <?php esc_html_e('Notice',  WPSCORE_LANG);?></option>
                                                            <option value="warning"><?php esc_html_e('Warning', WPSCORE_LANG);?></option>
                                                            <option value="error">  <?php esc_html_e('Error',   WPSCORE_LANG);?></option>
                                                        </select>
                                                    </form>
                                                </th>
                                                <th>
                                                    <form class="form-inline">
                                                    <?php esc_html_e('Product', WPSCORE_LANG);?>
                                                        <select name="logsProduct" class="form-control" v-model="filters.product">
                                                            <option value="">       <?php esc_html_e('All',     WPSCORE_LANG);?></option>
                                                            <option v-for="productName in products" v-bind:value="productName">{{productName}}</option>
                                                        </select>
                                                    </form>
                                                </th>
                                                <th>
                                                    <form class="form-inline"><?php esc_html_e('Message', WPSCORE_LANG);?>
                                                        <input type="text" class="form-control input-sm" placeholder="Filter messages" v-model="filters.message">
                                                    </form>
                                                </th>
                                                <th>
                                                    <form class="form-inline"><?php esc_html_e('Location', WPSCORE_LANG);?>
                                                        <input type="text" class="form-control input-sm" placeholder="Filter locations" v-model="filters.location">
                                                    </form>
                                                </th>
                                            </tr>
                                            <tr v-for="log in filteredlogs">
                                                <td><small>{{log.date}}</small></td>
                                                <td><span class="label" :class="log.class">{{log.type}}</span</td>
                                                <!--<td><small>{{log.from | capitalize}}</small></td>-->
                                                <td><small v-html="log.product"></small></td>
                                                <td><small v-html="log.message"></small></td>
                                                <td><small>{{log.file_uri}}:{{log.file_line}}</small></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </template>
                        </transition>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
}