<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_dashboard_page() {

    global $current_user;
    wp_get_current_user();
?>
<div id="wp-script">
    <div class="content-tabs" id="dashboard">
        <div class="row">
            <div class="col-xs-12 padding-top-10 padding-bottom-10">
                <div class="logo">
                    <h2><a href="https://www.wp-script.com/?utm_source=core&utm_medium=dashboard&utm_campaign=logo&utm_content=top" target="_blank"><img class="panda" src="<?php echo WPSCORE_URL; ?>admin/assets/images/logo-wp-script.svg" width="250"/></a></h2>
                </div>
            </div>
        </div>

        <?php WPSCORE()->display_tabs();?>

        <div class="tab-content">
            <div class="tab-pane active">

                <div v-cloak class="padding-top-15">

                    <div class="row text-center v-cloak--block">
                        <div class="col-xs-12 loading"><p><i class="fa fa-cog fa-spin fa-2x fa-fw" aria-hidden="true"></i><br><?php esc_html_e('Loading Core', WPSCORE_LANG); ?>...</span></p></div>
                    </div>

                    <div class="v-cloak--hidden">

                        <?php 
                        if ( !WPSCORE()->php_version_ok() ):?>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-push-3">
                                    <h3 class="text-center"><?php _e( 'PHP > 5.3.0 is required', WPSCORE_LANG );?></h3>
                                    <div class="alert alert-danger">
                                        <p><strong>PHP > 5.3.0</strong> <?php printf( __('is required to use WP-Script products. Your PHP version (%s) is too old. Please contact your host to update it or <a href="https://www.wp-script.com/help/?utm_source=core&utm_medium=dashboard&utm_campaign=help&utm_content=phpTooOld">send us a support ticket</a>', WPSCORE_LANG), PHP_VERSION );?></p>
                                    </div>
                                </div>
                            </div>
                        <?php else:?>
                            <!--**************-->
                            <!-- LOADING DATA -->
                            <!--**************-->
                            <template v-if="loading.loadingData">
                                <div class="row text-center">
                                    <div class="col-xs-12 loading"><p><i class="fa fa-cog fa-spin-reverse fa-2x fa-fw" aria-hidden="true"></i><br><?php esc_html_e('Loading Data', WPSCORE_LANG);?>...</span></p></div>
                                </div>
                            </template>

                            <transition name="fade">

                                <div v-if="dataLoaded">

                                    <div v-if="core !== false" class="row">
                                        <div class="col-xs-12">
                                            <p class="text-right">
                                                WP-Script Core v{{core.installed_version}}
                                                <template v-if="!core.is_latest_version">
                                                    <button v-if="loading.updatingCore == false" @click="updateCore" class="btn btn-default btn-sm btn-success"><i class="fa fa-refresh" aria-hidden="true"></i> Update to v{{core.latest_version}}</button>
                                                    <button v-if="loading.updatingCore == true" class="btn btn-default btn-sm btn-success disabled" disabled><i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i> Updating to v{{core.latest_version}}</button>
                                                    <button v-if="loading.updatingCore == 'activating'" class="btn btn-default btn-sm btn-success disabled" disabled><i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i> Activating...</button>
                                                    <button v-if="loading.updatingCore == 'reloading'" class="btn btn-default btn-sm btn-success disabled" disabled><i class="fa fa-cog fa-spin-reverse fa-fw" aria-hidden="true"></i> Reloading...</button>
                                                </template>
                                            </p>
                                        </div>
                                    </div>

                                    <!--***********************-->
                                    <!-- LICENSE NOT ACTIVETED -->
                                    <!--***********************-->
                                    <div v-if="!userLicense || loading.checkingLicense == 'reloading'">
                                        
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="alert text-center" :class="licenceBoxClass">
                                                    <h3>
                                                        <template v-if="loading.checkingLicense != 'reloading' && loading.checkingAccount != 'reloading' && !error"><?php esc_html_e("Activation required", WPSCORE_LANG);?></template>
                                                        <template v-if="error">{{error}}</template>
                                                        <template v-if="loading.checkingLicense == 'reloading' || loading.checkingAccount == 'reloading'"><?php esc_html_e("Activation Successful", WPSCORE_LANG);?> - <?php esc_html_e("Reloading", WPSCORE_LANG);?>...</template>
                                                    </h3>
                                                    <form action="" method="post">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-6 col-lg-6 col-lg-push-3">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                                    <input v-model="userLicenseInput" type="text" class="form-control check-license" placeholder="<?php esc_html_e('Paste your WP-Script License Key here', WPSCORE_LANG);?>" ref="refLicenseInput" />
                                                                    <div class="input-group-btn">
                                                                        <button @click.prevent="checkLicense" class="btn btn-default" :disabled="toggleLicenseBtn" type="submit">
                                                                            <template v-if="loading.checkingLicense === true">
                                                                                <i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i> <?php esc_html_e('Activating License', WPSCORE_LANG);?>... 
                                                                            </template>
                                                                            <template v-if="loading.checkingLicense == 'reloading'"><i class="fa fa-check text-success" aria-hidden="true"></i></template>
                                                                            <template v-if="loading.checkingLicense === false"><?php esc_html_e('Activate my WP-Script License', WPSCORE_LANG);?></template>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <p class="margin-top-10 margin-bottom-20"><small><a href="https://www.wp-script.com/my-account/?utm_source=core&utm_medium=dashboard&utm_campaign=account&utm_content=getLicenseKey" title="<?php esc_html_e('Go to your WP-Script account to get your license key', WPSCORE_LANG);?>" target="_blank"><?php esc_html_e('Go to your WP-Script account to get your license key', WPSCORE_LANG);?></a></small></p>

                                                    <button class="btn btn-transparent" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

                                                    <?php esc_html_e("You don't have a WP-Script license yet?", WPSCORE_LANG);?>
                                                    </button>
                                                    <div class="collapse" id="collapseExample">
                                                        <form id="form-check-account" action="" method="post">
                                                            <div class="row padding-top-20">
                                                                <div class="col-xs-12 col-md-6 col-lg-6 col-lg-push-3">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                                                        <input v-model="userEmailInput" type="text" class="form-control" value="" ref="refEmailInput"/>
                                                                        <div class="input-group-btn">
                                                                            <button @click.prevent="checkAccount" class="btn btn-default" type="submit">
                                                                                <template v-if="loading.checkingAccount === true">
                                                                                    <i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i> <?php esc_html_e('Creating account', WPSCORE_LANG); ?>... 
                                                                                </template>
                                                                                <template v-if="loading.checkingAccount == 'reloading'"><i class="fa fa-check text-success" aria-hidden="true"></i></template>
                                                                                <template v-if="loading.checkingAccount === false"><?php esc_html_e('Create my WP-Script Account', WPSCORE_LANG); ?></template>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <p class="margin-top-10"><small><a rel="tooltip" data-html="true" data-original-title="<ul><li>–</li><li><?php esc_html_e("You're going to create your unique WP-Script account", WPSCORE_LANG); ?></li><li>–</li><li><?php esc_html_e("You will receive your login details to this email address", WPSCORE_LANG); ?></li><li>–</li><li><?php esc_html_e('No spam', WPSCORE_LANG); ?></li><li>–</li></ul>"><i class="fa fa-question-circle" aria-hidden="true"></i></span> <?php esc_html_e('More informations', WPSCORE_LANG); ?></a></small></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <!--*******************-->
                                    <!-- LICENSE ACTIVATED -->
                                    <!--*******************-->
                                    <div v-else>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="alert text-center" :class="[wpsGold.site_connected ? 'wps-gold wps-gold-connected' : 'alert-info']">
                                                    <template v-if="wpsGold.site_connected">
                                                        <span class="text-gold wps-gold-text"><i class="fa fa-trophy" aria-hidden="true"></i> <?php _e("Site connected with WP-Script Gold", WPSCORE_LANG);?></span>
                                                    </template>

                                                    <p><?php _e("Your WP-Script License Key", WPSCORE_LANG);?></p>
                                                    <div class="row padding-top-10 padding-bottom-10">
                                                        <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control text-center" id="input-license" v-model="userLicenseInput" ref="refLicenseInput">
                                                                <span class="input-group-btn">
                                                                    <button @click.prevent="checkLicense" class="btn btn-default" :class="{'disabled' : !userLicenseChanged}" :disabled="!userLicenseChanged" type="button"><i class="fa" :class="licenseButtonIconClass" aria-hidden="true"></i></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div v-if="!wpsGold.site_connected" class="wps-gold wps-gold-not-connected text-center">
                                            <h2><i class="fa fa-trophy text-gold" aria-hidden="true"></i> <?php _e("WP-Script Gold", WPSCORE_LANG);?></h2>
                                            <p><a href="https://www.wp-script.com/gold/?utm_source=core&utm_medium=dashboard&utm_campaign=gold&utm_content=aboutGold" target="_blank"><?php _e("About WP-Script Gold", WPSCORE_LANG);?> <i class="fa fa-external-link" aria-hidden="true"></i></a></p>
                                            <p>{{wpsGoldSiteState.paragraph}}</p>
                                            <p>
                                                <template v-if="wpsGoldSiteState.linkHref">
                                                    <a :href="wpsGoldSiteState.linkHref" class="btn btn-gold" target="_blank">
                                                        <i :class="wpsGoldSiteState.iconClass" aria-hidden="true"></i> {{wpsGoldSiteState.buttonText}}
                                                    </a>
                                                </template>
                                                <template v-else>
                                                    <button @click.prevent="wpsGoldConnectSite" class="btn btn-gold">
                                                        <i :class="wpsGoldSiteState.iconClass" aria-hidden="true"></i> {{wpsGoldSiteState.buttonText}}
                                                    </button>
                                                </template>
                                                
                                            </p>
                                        </div>

                                        <products v-if="productsFromApi.themes" :products="productsFromApi.themes" type="theme" :installed-products="installedProducts['themes']" :user-license="userLicense" :wps-gold-site-connected="wpsGold.site_connected" @show-requirements-modal="showRequirementsModal" @show-install-modal="showInstallModal" @show-info-modal="showConnectionInfosModal"></products>
                                        <products v-if="productsFromApi.plugins" :products="productsFromApi.plugins" type="plugin" :installed-products="installedProducts['plugins']" :user-license="userLicense" :wps-gold-site-connected="wpsGold.site_connected" @show-requirements-modal="showRequirementsModal" @show-install-modal="showInstallModal"  @show-info-modal="showConnectionInfosModal"></products>
                                    </div>
                                </div>
                            </transition>
                        <?php endif;?>
                    </div>
                    <p class="text-right"><small>cUrl v<?php echo WPSCORE()->get_curl_version();?></small></p>
                </div>
            </div>
        </div>
        <div class="full-block-white margin-top-10 text-center"><i class="fa fa-heart pink" aria-hidden="true"></i> <?php esc_html_e('Thank you for using', WPSCORE_LANG); ?> <strong><a target="_blank" href="https://www.wp-script.com/?utm_source=core&utm_medium=dashboard&utm_campaign=thankyou&utm_content=footer">WP-Script</a></strong>! </div>
        

        <!-- Create Connection Infos Modal -->
        <div class="modal fade" id="connection-infos-modal">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php esc_html_e('Close', WPSCORE_LANG); ?></span></button>
                        <h4 class="modal-title text-center"><?php esc_html_e('How WP-Script products connection works?', WPSCORE_LANG); ?></h4>
                    </div>
                    <div class="modal-body text-center">
                        <div class="row">
                            <p><?php esc_html_e( 'The button bellow each product will automatically change depending on which step you are.', WPSCORE_LANG);?></p>
                            <div class="col-xs-12 col-md-6">
                                <div class="thumbnail">
                                    <img src="<?php echo WPSCORE_URL; ?>admin/assets/images/product-connection-step-1.jpg">
                                    <div class="caption"><h5><strong>1.</strong> <?php esc_html_e( 'Purchase any product you need for 1 / 5 / ∞ sites', WPSCORE_LANG);?></h5></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="thumbnail">
                                    <img src="<?php echo WPSCORE_URL; ?>admin/assets/images/product-connection-step-2.jpg">
                                    <div class="caption"><h5><strong>2.</strong> <?php esc_html_e('Connect the purchased product', WPSCORE_LANG);?></h5></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="thumbnail">
                                    <img src="<?php echo WPSCORE_URL; ?>admin/assets/images/product-connection-step-3.jpg">
                                    <div class="caption"><h5><strong>3.</strong> <?php esc_html_e('Install the purchased product', WPSCORE_LANG);?></h5></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="thumbnail">
                                    <img src="<?php echo WPSCORE_URL; ?>admin/assets/images/product-connection-step-4.jpg">
                                    <div class="caption"><h5><strong>4.</strong> <?php esc_html_e('Activate the purchased product', WPSCORE_LANG);?></h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Create Connection Infos Modal -->

        <!-- Create Requirements Infos Modal -->
        <div class="modal fade" id="requirements-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php esc_html_e('Close', WPSCORE_LANG); ?></span></button>
                        <h4 class="modal-title text-center"><?php esc_html_e('Requirements', WPSCORE_LANG); ?> for {{currentProduct.infos.title}}</h4>
                    </div>
                    <div class="modal-body">
                        <div v-if="currentProduct.infos.requirements == ''" class="alert alert-success text-center">
                            <p><?php esc_html_e('There is no requirement to use this product. This product will work properly.', WPSCORE_LANG); ?></p>
                        </div>
                        <template v-else>
                            <p class="text-center"><?php esc_html_e('These following PHP elements must be installed on your server to use this product', WPSCORE_LANG); ?></p>
                            
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Installed</th>
                                </tr>
                                <tr v-for="requirement in currentProduct.infos.requirements">
                                    <td>
                                        <strong>{{requirement.name}}</strong>
                                    </td>
                                    <td>
                                        PHP {{requirement.type}}
                                    </td>
                                    <td>
                                        <span v-if="requirement.status" class="text-success">
                                            <i class="fa fa-check" aria-hidden="true"></i> <?php esc_html_e('yes', WPSCORE_LANG); ?>
                                        </span>
                                        <span v-else  class="text-danger">
                                            <i class="fa fa-times" aria-hidden="true"></i> <?php esc_html_e('no', WPSCORE_LANG); ?>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                            <div v-if="currentProduct.allRequirementsOk" class="alert alert-success text-center">
                                <p><?php esc_html_e('All required PHP elements are installed on your server. This product will work properly.', WPSCORE_LANG); ?></p>
                            </div>
                            <div v-else class="alert alert-danger text-center">
                                <p><?php esc_html_e('Some PHP elements are not installed on your server. This product may not work properly. Please contact your web hoster.', WPSCORE_LANG); ?></p>
                            </div>
                        </template>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Create Requirements Infos Modal -->

        <!-- Create Connection Infos Modal -->
        <div class="modal fade" id="install-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php esc_html_e('Close', WPSCORE_LANG); ?></span></button>
                        <h4 class="modal-title text-center"><?php esc_html_e('An error occured while installing or updating', WPSCORE_LANG); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="alert alert-danger text-center">
                                    <?php esc_html_e('Your server configuration prevents the product to be installed automatically.', WPSCORE_LANG); ?>
                                </div>
                                <template v-if="!installModal.showMoreInfos">
                                    <p class="text-center">
                                        <a href="#" @click.prevent="installModal.showMoreInfos = true">Show more infos</a>
                                    </p>
                                </template>
                                <template v-else>
                                    <p class="text-center">
                                        <a href="#" @click.prevent="installModal.showMoreInfos = false">Hide more infos</a>
                                    </p>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            {{installModal.message}}
                                        </div>
                                    </div>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Create Connection Infos Modal -->
    </div>
</div>

<?php 
}
