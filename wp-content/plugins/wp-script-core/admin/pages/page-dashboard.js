//jetpack fix
_.contains = _.includes;
window.lodash = _.noConflict();

window.onload = function () {

    jQuery('body').tooltip({ selector: '[rel=tooltip]', container: 'body', trigger : 'hover' });

    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

    if(document.getElementById('dashboard')){
        var dashboard = new Vue({
            el: '#dashboard',
            data: {
                error               : '',
                loading             : {
                    checkingAccount     : false,
                    checkingLicense     : false,
                    loadingData         : false,
                    updatingCore        : false,
                    connectingSite      : false
                },
                dataLoaded          : false,
                userID              : 0,
                userLicense         : '',
                userLicenseInput    : '',
                userEmail           : '',
                userEmailInput      : '',
                productsFromApi     : {},
                installedProducts   : {},
                core                : {},
                wpsGold             : false,
                currentProduct      :{
                    infos: [],
                    allRequirementsOk: false
                },
                installModal: {
                    message: '',
                    showMoreInfos: false
                },

            },
            mounted: function(){
                this.loadData();
            },
            computed: {
                toggleLicenseBtn : function (){
                    return this.userLicenseInput.length == 0;
                },
                userLicenseChanged : function(){
                    return this.userLicense !== this.userLicenseInput;
                },
                licenseButtonIconClass : function (){
                    if( !this.userLicenseChanged ){
                        return 'fa-check text-success';
                    }
                    else if( this.loading.checkingLicense ){
                        return 'fa-cog fa-spin';
                    }else if( this.error != ''){
                        return 'fa-times text-danger';
                    }else{
                        return 'fa-refresh text-primary';
                    }
                },
                licenceBoxClass: function(){
                    if( this.loading.checkingLicense == 'reloading' ) return 'alert-success';
                    if( this.error ) return 'alert-danger';
                    return 'alert-info';
                },
                wpsGoldTimeRemainingInDays: function () {
                    var timeRemainingInSeconds = parseInt(this.wpsGold.time_remaining, 10);
                    return Math.floor(timeRemainingInSeconds / (3600*24));
                  },
                wpsGoldSiteState: function() {
                    var siteState = {
                        paragraph: '',
                        linkHref: '',
                        buttonClass: '',
                        buttonText: '',
                        iconClass: ''
                    };

                    if( this.wpsGold.site_connected ) return siteState;
                    
                    if (this.wpsGold.time_remaining <= 0) {
                        if( this.wpsGold.current_plan ) {
                            siteState.paragraph = 'Your WP-Script Gold subscription has expired. Reactivate it and get instant access to all existing and future WP-Script products';
                            siteState.linkHref = 'https://www.wp-script.com/gold/?utm_source=core&utm_medium=dashboard&utm_campaign=gold&utm_content=reJoin';
                            siteState.buttonText = 'Reactivate WP-Script Gold';
                            siteState.iconClass = ['fa', 'fa-trophy'];
                        } else {
                            siteState.paragraph = 'Join WP-Script Gold and get access to all existing and future WP-Script products';
                            siteState.linkHref = 'https://www.wp-script.com/gold/?utm_source=core&utm_medium=dashboard&utm_campaign=gold&utm_content=join';
                            siteState.buttonText = 'Join WP-Script Gold';
                            siteState.iconClass = ['fa', 'fa-trophy'];
                        }
                        
                    } else {
                        if (this.wpsGold.sites_remaining > 0) {
                            siteState.paragraph = 'Connect this site with your WP-Script Gold subscription and get instant access to all existing and future WP-Script products';
                            switch ( this.loading.connectingSite ) {
                                case false:
                                    siteState.buttonText = 'Connect this site';
                                    siteState.iconClass = ['fa', 'fa-plug'];
                                break;
                                case true:
                                    siteState.buttonText = 'Connecting site';
                                    siteState.iconClass = ['fa', 'fa-cog', 'fa-spin', 'fa-fw'];
                                    break;
                                
                                case 'reloading': 
                                    siteState.buttonText = 'Reloading';
                                    siteState.iconClass = ['fa', 'fa-cog', 'fa-spin-reverse', 'fa-fw'];
                                    break;
                                default:
                                    break;
                            }                                
                        } else {
                            if (this.wpsGold.current_plan < 25) {
                                siteState.paragraph = 'You\'ve reached the limit of sites (' + this.wpsGold.current_plan + ') you can connect with your current WP-Script Gold plan';
                                siteState.linkHref = 'https://www.wp-script.com/gold/?utm_source=core&utm_medium=dashboard&utm_campaign=gold&utm_content=upgrade';
                                siteState.buttonText = 'Upgrade WP-Script Gold plan';
                                siteState.iconClass = ['fa', 'fa-shopping-cart'];
                            } else {
                                siteState.paragraph = 'You\'ve reached the limit of sites (' + this.wpsGold.current_plan + ') you can connect with the WP-Script Gold';
                                siteState.linkHref = 'https://www.wp-script.com/contact/?utm_source=core&utm_medium=gold&utm_campaign=gold&utm_content=25sitesReached';
                                siteState.buttonText = 'Contact us';
                                siteState.iconClass = ['fa', 'fa-comments-o'];
                            }
                        }
                    }
                    return siteState;
                }
            },
            methods: {
                loadData: function(){
                    this.loading.loadingData = true;
                    this.$http.post(
                        WPSCORE_dashboard.ajax.url,
                        {
                            action        : 'WPSCORE_load_dashboard_data',
                            nonce         : WPSCORE_dashboard.ajax.nonce
                        },
                        {
                            emulateJSON: true
                        })
                    .then((response) => {
                        // success callback
                        this.userLicense        = this.userLicenseInput   = response.body.user_license;
                        this.userEmail          = this.userEmailInput     = response.body.user_email;
                        this.productsFromApi    = response.body.products;
                        this.core               = response.body.core;
                        this.wpsGold            = response.body.wps_gold;

                        lodash.each( this.productsFromApi, function(productsByType){
                            lodash.each( productsByType, function( product ){
                                product.show = true;
                            });
                        });
                        this.installedProducts  = response.body.installed_products;
                    }, (response) => {
                        // error callback
                        console.error(response);
                    }).then( _ => {
                        this.loading.loadingData = false;
                        this.dataLoaded = true;
                    });
                },
                checkLicense: function(){
                    this.loading.checkingLicense = true;
                    var savedLicenseInput = this.userLicense;
                    this.$http.post(
                        WPSCORE_dashboard.ajax.url,
                        {
                            action        : 'WPSCORE_check_license_key',
                            nonce         : WPSCORE_dashboard.ajax.nonce,
                            license_key   : this.userLicenseInput
                        },
                        {
                            emulateJSON: true
                        })
                    .then((response) => {
                        // success callback
                        if( response.body.code === 'success' ){
                            this.userLicense = this.userLicenseInput;

                            this.loading.checkingLicense = 'reloading';
                            //this.loadData();
                            document.location.href = 'admin.php?page=WPSCORE-dashboard';

                        }else if( response.body.code === 'error' ){
                            this.error = this.userLicenseInput = response.body.message;
                            var self = this;
                            setTimeout(function() {
                                self.userLicenseInput = savedLicenseInput;
                                self.error = '';
                                self.$refs.refLicenseInput.focus();
                            }, 3000);
                        }else{
                            this.error = this.userLicenseInput = 'Invalid License Key';
                            var self = this;
                            setTimeout(function() {
                                self.userLicenseInput = savedLicenseInput;
                                self.error = '';
                                self.$refs.refLicenseInput.focus();
                            }, 3000);
                        }
                    }, (response) => {
                        // error callback
                    }).then( _ => {
                        this.loading.checkingLicense = false;

                    });
                },
                checkAccount: function(){
                    this.loading.checkingAccount = true;
                    var savedEmailInput = this.userEmail;
                    this.$http.post(
                        WPSCORE_dashboard.ajax.url,
                        {
                            action  : 'WPSCORE_check_account',
                            nonce   : WPSCORE_dashboard.ajax.nonce,
                            email   : this.userEmailInput
                        },
                        {
                            emulateJSON: true
                        })
                    .then((response) => {
                        // success callback
                        if( response.body.code === 'success' ){

                            this.loading.checkingAccount = 'reloading';
                            this.loading.checkingLicense = 'reloading';

                            this.userLicense = this.userLicenseInput = response.body.data.license

                            setTimeout(function() {
                                document.location.href = 'admin.php?page=WPSCORE-dashboard';
                            }, 3000);

                        }else if( response.body.code === 'error' ){
                            this.error = this.userEmailInput = response.body.message;
                            var self = this;
                            setTimeout(function() {
                                self.userEmailInput = savedEmailInput;
                                self.error = '';
                                self.$refs.refEmailInput.focus();
                                self.loading.checkingAccount = false;
                            }, 3000);
                        }else{
                            this.error = this.userEmailInput = 'Invalid License Key';
                            var self = this;
                            setTimeout(function() {
                                self.userEmailInput = savedEmailInput;
                                self.$refs.refEmailInput.focus();
                                self.loading.checkingAccount = false;
                            }, 3000);
                        }
                    }, (response) => {
                        // error callback
                    }).then( _ => {
                    });
                },
                updateCore: function(){
                    var self = this;
                    this.loading.updatingCore = true;
                    this.$http.post(
                        WPSCORE_dashboard.ajax.url,
                        {
                            action              : 'WPSCORE_install_product',
                            nonce               : WPSCORE_dashboard.ajax.nonce,
                            product_sku         : this.core.sku,
                            product_type        : 'plugin',
                            product_zip         : this.core.zip_file,
                            product_slug        : this.core.slug,
                            product_folder_slug : this.core.folder_slug,
                            method              : 'upgrade',
                            new_version         : this.core.latest_version
                        },
                        {
                            emulateJSON: true
                        })
                    .then((response) => {
                        // success callback
                        if( response.body === true || response.body == '<div class="wrap"><h1></h1></div>' ){
                            this.loading.updatingCore   = 'reloading';
                            document.location.href      = 'admin.php?page=WPSCORE-dashboard';
                        }else{
                            this.showInstallModal(response.body);
                        }
                    }, (response) => {
                        // error callback
                    }).then( _ => {
                    });
                },
                wpsGoldConnectSite : function(){
                    this.loading.connectingSite = true;
                    this.$http.post(
                        WPSCORE_dashboard.ajax.url,
                        {
                            action          : 'WPSCORE_wpsgold_connect_site',
                            nonce           : WPSCORE_dashboard.ajax.nonce
                        },
                        {
                            emulateJSON: true
                        })
                    .then((response) => {
                        // success callback
                        if( response.body.code == 'error' ){
                            console.error(response.body.message);
                        }else{
                            //this.productFromApi.status = response.body.data.product_status;
                            this.loading.connectingSite = 'reloading';
                            document.location.href  = 'admin.php?page=WPSCORE-dashboard';
                        }
                    }, (response) => {
                        // error callback
                    }).then( _ => {
                        this.connectSite = false;
                    });
                },
                toggleFilter: function( productType, newValue ){
                    this.filters[productType] = newValue;
                        var self = this;
                    lodash.each(this.productsFromApi[productType], function(productFromApi){
                        var productSku = productFromApi.sku;
                             switch( newValue ){
                            case 'All':
                                productFromApi.show = true;
                                break;
                            case 'Connected':
                                productFromApi.show = productFromApi.status  == 'connected';
                                break;
                            case 'Not connected':
                                productFromApi.show = productFromApi.status  != 'connected';
                                break;
                            case 'Installed':
                                productFromApi.show = self.installedProduct !== undefined;
                                break;
                            case 'Not installed':
                                productFromApi.show = self.installedProduct === undefined;
                                break;
                            case 'Activated':
                                productFromApi.show = lodash.has(self.installedProducts[productType][productSku], 'state') && self.installedProducts[productType][productSku].state == 'activated';
                                break;
                            case 'Not activated':
                                productFromApi.show = !lodash.has(self.installedProducts[productType][productSku], 'state') ||( lodash.has(self.installedProducts[productType][productSku], 'state') && self.installedProducts[productType][productSku].state != 'activated' );
                                break;
                            default:
                                productFromApi.show = false;
                                break;
                        }
                    });
                },
                showRequirementsModal: function( productInfos, allRequirementsOk ){
                    this.currentProduct.infos = productInfos;
                    this.currentProduct.allRequirementsOk = allRequirementsOk;
                    jQuery('#requirements-modal').modal('show');
                },
                showInstallModal: function( installInfos ){
                    this.installModal.message = installInfos;
                    jQuery('#install-modal').modal('show');
                },
                showConnectionInfosModal: function(){
                    jQuery('#connection-infos-modal').modal('show');
                }
            },
            components: {
                'products': {
                    props : ['products', 'type', 'installedProducts', 'userLicense', 'wpsGoldSiteConnected'],
                    data: function(){
                        return {
                            filter : 'All'
                        }
                    },
                    template : `
                        <div class="row">
                            <h3>{{type | titled}}
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{filter}} <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" v-on:click.prevent="toggleFilter('All')">All</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#" v-on:click.prevent="toggleFilter('Connected')">Connected</a></li>
                                        <li><a href="#" v-on:click.prevent="toggleFilter('Not connected')">Not connected</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#" v-on:click.prevent="toggleFilter('Installed')">Installed</a></li>
                                        <li><a href="#" v-on:click.prevent="toggleFilter('Not installed')">Not installed</a></li>
                                    </ul>
                                </div>
                            </h3>
                            <div class="products">
                                <div v-for="product in products" class="col-xs-12 col-md-6 col-lg-3 product" v-bind:class="{'product-hide':!product.show}">
                                    <product 
                                        v-bind:product-type="type" 
                                        v-bind:product-from-api="product" 
                                        v-bind:installed-product="installedProducts[product.sku]" 
                                        v-bind:user-license="userLicense" 
                                        v-bind:wps-gold-site-connected="wpsGoldSiteConnected" 
                                        v-on:show-requirements-modal="showRequirementsModal" 
                                        v-on:show-info-modal="showConnectionInfosModal" 
                                        v-on:show-install-modal="showInstallModal">
                                    </product>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    `,
                    filters : {
                        titled(value){
                            return value.charAt(0).toUpperCase() + value.slice(1) + 's';
                        }
                    },
                    methods: {
                        toggleFilter: function( newValue ){
                            this.filter = newValue;
                            var self = this;
                            lodash.each(this.products, function(productFromApi){
                                var productSku = productFromApi.sku;
                                switch( newValue ){
                                    case 'All':
                                        productFromApi.show = true;
                                        break;
                                    case 'Connected':
                                        productFromApi.show = productFromApi.status  == 'connected';
                                        break;
                                    case 'Not connected':
                                        productFromApi.show = productFromApi.status  != 'connected';
                                        break;
                                    case 'Installed':
                                        productFromApi.show = lodash.has(self.installedProducts, productSku);
                                        break;
                                    case 'Not installed':
                                        productFromApi.show = !lodash.has(self.installedProducts, productSku);
                                        break;
                                    default:
                                        productFromApi.show = false;
                                        break;
                                }
                            });
                        },
                        showRequirementsModal: function(productInfos, allRequirementsOk){
                            this.$emit('show-requirements-modal', productInfos, allRequirementsOk);
                        },
                        showInstallModal: function(installInfos){
                            this.$emit('show-install-modal', installInfos);
                        },
                        showConnectionInfosModal: function(){
                            this.$emit('show-info-modal');
                        }
                    },
                    components: {
                        'product': {
                            props: ['productType', 'productFromApi', 'installedProduct', 'userLicense', 'wpsGoldSiteConnected' ],
                            data: function(){
                                return {
                                    loading : {
                                        connect : false,
                                        install : false,
                                        toggle  : false
                                    },
                                    showPopOver : false,
                                   //plan: lodash.has( this.productFromApi.variations[0], 'sku' ) ? this.productFromApi.variations[0].sku : '',
                                    currentUrl : window.location.hostname
                                }
                            },
                            template: `
                                <div>
                                    <div v-if="productIs.debug" class="product-square product-coming-soon">
                                        <div class="ribon-coming-soon"></div>
                                        <div class="product-image">
                                            <img class="img-responsive " v-bind:src="productFromApi.thumb_url" v-bind:alt="productFromApi.title" />
                                        </div>
                                        <p class="product-exerpt">{{productFromApi.exerpt}}</p>
                                        <div class="product-footer">
                                            <button class="btn btn-default disabled" disabled><i class="fa fa-fire" aria-hidden="true"></i> Coming Soon</button>
                                        </div>
                                    </div>

                                    <div v-else class="product-square" v-bind:class="{'product-connected':productIs.connected}">

                                        <div class="product-requirements" v-on:click="showRequirementsModal(productFromApi, allRequirementsOk)">
                                            <small>Requirements 
                                            <i v-if="allRequirementsOk" class="fa fa-check text-success" aria-hidden="true"></i>
                                            <i v-else class="fa fa-exclamation-triangle text-danger" aria-hidden="true"></i>
                                            </small> 
                                        </div>

                                        <template v-if="!wpsGoldSiteConnected">
                                            <div v-if="productIs.connected" class="product-connection text-success" v-on:click="showConnectionInfosModal">
                                                <small><i class="fa fa-circle" aria-hidden="true"></i> Connected</small>
                                            </div>

                                            <div v-else class="product-connection text-danger" v-on:click="showConnectionInfosModal">
                                                <small><i class="fa fa-circle" aria-hidden="true"></i> Not Connected</small>
                                            </div>
                                        </template>

                                        <div class="product-image">
                                            <img class="img-responsive" v-bind:src="productFromApi.thumb_url" v-bind:alt="productFromApi.title" />
                                        </div>

                                        <p class="product-exerpt">{{productFromApi.exerpt}} – <a href="#" v-bind:href="'https://www.wp-script.com/' + productType + 's/' + productFromApi.slug + '/?utm_source=core&utm_medium=dashboard&utm_campaign=' + productFromApi.slug + '&utm_content=learnMore'" target="_blank" v-bind:title="'View details about ' + productFromApi.title">Learn more <i class="fa fa-external-link" aria-hidden="true"></i></a></p>

                                        <div class="product-footer">
                                            
                                            <template v-if="!productIs.installed && (productIs.connected || productIs.freemium)">
                                                <button v-if="!loading.toggle && !loading.install" v-on:click.prevent="installProduct('install')" class="btn btn-default" v-bind:title="'Install ' + productFromApi.title"><i class="fa fa-download" aria-hidden="true"></i> Install</button>

                                                <button v-if="!loading.toggle && loading.install" class="btn btn-default disabled" disabled v-bind:title="'Installing ' + productFromApi.title"><i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i> Installing...</button>

                                                <button v-if="loading.toggle == true" class="btn btn-default disabled" disabled v-bind:title="'Activating ' + productFromApi.title"><i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i> Activating...</button>

                                                <button v-if="loading.toggle == 'reloading'" class="btn btn-default disabled" disabled v-bind:title="'Reloading'" target="_blank"><i class="fa fa-cog fa-spin-reverse fa-fw" aria-hidden="true"></i> Reloading...</button>

                                            </template>
                                           
                                            <template v-if="!productIs.connected">

                                                <template v-if="productFromApi.connectable_sites >= 1 || productFromApi.connectable_sites == 'unlimited'">
                                                    <button v-on:click.prevent="togglePopOver" class="btn btn-success" v-bind:title="'Connect ' + productFromApi.title + ' on this domain'" target="_blank"><i class="fa fa-plus-circle" aria-hidden="true"></i> Connect &nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></button>
                                                </template>

                                                <template v-else>
                                                    <a v-bind:href="'https://www.wp-script.com/' + productType + 's/' + productFromApi.slug + '/' + autoConnect" target="_blank"  class="btn btn-pink" v-bind:title="'Buy ' + productFromApi.title">
                                                        <i class="fa fa-shopping-cart"></i>&nbsp;
                                                        Purchase
                                                    </a>
                                                </template>
                                            </template>

                                            <template v-if="productIs.connected && productIs.installed">
                                                <p>
                                                    <span class="text-success">Installed</span> v{{installedProduct.installed_version}}
                                                    <template v-if="productIs.updatable">
                                                        &nbsp;|&nbsp;
                                                        <a v-if="!loading.install" class="btn btn-default btn-sm btn-success" href="#" v-on:click.prevent="installProduct('upgrade')"><i aria-hidden="true" class="fa fa-refresh"></i> Update to v{{productFromApi.latest_version}}</a>
                                                        <a v-else class="btn btn-default btn-sm btn-success disabled" disabled href="#"><i aria-hidden="true" class="fa fa-cog fa-spin fa-fw"></i> Updating to v{{productFromApi.latest_version}}...</a>
                                                    </template>
                                                    <template v-if="installedProduct.state == 'deactivated'">
                                                        &nbsp;|&nbsp;
                                                        <template v-if="loading.toggle == false"><a href="#" v-on:click.prevent="toggleProduct">Activate</a></template>
                                                        <template v-if="loading.toggle == true"><i class="fa fa-cog fa-spin fa-fw"></i> Activating...</template>
                                                    </template>
                                                    <template v-if="installedProduct.state == 'activated' && productType != 'theme'">
                                                        &nbsp;|&nbsp;
                                                        <template v-if="loading.toggle == false"><a href="#" v-on:click.prevent="toggleProduct">Deactivate</a></template>
                                                        <template v-if="loading.toggle == true"><i class="fa fa-cog fa-spin fa-fw"></i> Deactivating...</template>
                                                    </template>
                                                    <template v-if="loading.toggle == 'reloading'"><i class="fa fa-cog fa-spin-reverse fa-fw"></i> Reloading...</template>
                                                </p>
                                            </template>

                                        </div>

                                        <div class="product-over" v-bind:class="{show:showPopOver}">
                                            <template v-if="productIs.connected && !productIs.installed">
                                                <p>Install {{productFromApi.title}} to use it</p>
                                            </template>

                                            <template v-if="!productIs.connected">
                                                <template v-if="productFromApi.connectable_sites >= 1 || productFromApi.connectable_sites == 'unlimited'">
                                                    <p>Connect <strong>{{productFromApi.title}}</strong> on <code>{{currentUrl}}</code>
                                                    <template v-if="productFromApi.connectable_sites !== 'unlimited'"><br><small>Connecting will decrease your sites left by 1</small></template>
                                                    <br><span class="text-success">Sites left: <strong>{{productFromApi.connectable_sites}}</strong></span></p>
                                                    <div class="product-footer">
                                                        <button v-on:click.prevent="togglePopOver" class="btn btn-default"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                                                        <button  v-if="loading.connect == false" v-on:click.prevent="connectProduct" class="btn btn-success" v-bind:title="'Connect ' + productFromApi.title + ' on this domain'" target="_blank"><i class="fa fa-plus-circle" aria-hidden="true"></i> Connect now</button>
                                                        <button v-else class="btn btn-success disabled" disabled v-bind:title="'Connecting ' + productFromApi.title + ' on this domain'" target="_blank"><i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i> Connecting...</button>
                                                    </div>
                                                </template>
                                            </template>
                                        </div>
                                    </div>
                                </div>
                            `,
                            computed:{
                                autoConnect : function(){
                                    return '?ac=' + Base64.encode( this.userLicense );
                                },
                                productIs : function(){
                                    return  {
                                        activated : lodash.has(this.installedProduct, 'state') && this.installedProduct.state == 'activated',
                                        connected : this.productFromApi.status  == 'connected',
                                        debug     : this.productFromApi.debug,
                                        freemium  : this.productFromApi.model   == 'freemium',
                                        installed : this.installedProduct       !== undefined,
                                        updatable : lodash.has(this.installedProduct, 'installed_version') && this.installedProduct.installed_version != this.productFromApi.latest_version
                                    }
                                },
                                allRequirementsOk : function(){
                                    var output = true;
                                    lodash.each( this.productFromApi.requirements, function(r){
                                        if( r.status === false ){
                                            output = false;
                                        }
                                    });
                                    return output;
                                }
                            },
                            methods:{
                                choosePlan : function( plan ){
                                    this.plan = plan;
                                },
                                togglePopOver : function(){
                                    this.showPopOver = !this.showPopOver;
                                },
                                connectProduct : function(){
                                    var self = this;
                                    this.loading.connect = true;
                                    this.$http.post(
                                        WPSCORE_dashboard.ajax.url,
                                        {
                                            action          : 'WPSCORE_connect_product',
                                            nonce           : WPSCORE_dashboard.ajax.nonce,
                                            product_sku     : this.productFromApi.sku,
                                            product_title   : this.productFromApi.title
                                        },
                                        {
                                            emulateJSON: true
                                        })
                                    .then((response) => {
                                        // success callback
                                        if( response.body.code == 'error' ){
                                            console.error(response.body.message);
                                        }else{
                                            //this.productFromApi.status = response.body.data.product_status;
                                            this.loading.connect    = 'reloading';
                                            document.location.href  = 'admin.php?page=WPSCORE-dashboard';
                                        }
                                    }, (response) => {
                                        // error callback
                                    }).then( _ => {

                                    });
                                },
                                installProduct: function( method ){
                                    var self = this;
                                    this.loading.install = true;
                                    this.$http.post(
                                        WPSCORE_dashboard.ajax.url,
                                        {
                                            action              : 'WPSCORE_install_product',
                                            nonce               : WPSCORE_dashboard.ajax.nonce,
                                            product_sku         : this.productFromApi.sku,
                                            product_type        : this.productType,
                                            product_zip         : this.productFromApi.zip_file,
                                            product_slug        : this.productFromApi.slug,
                                            product_folder_slug : this.productFromApi.folder_slug,
                                            method              : method,
                                            new_version         : this.productFromApi.latest_version
                                        },
                                        {
                                            emulateJSON: true
                                        })
                                    .then((response) => {
                                        // success callback
                                        if( response.body === true || response.body == '<div class="wrap"><h1></h1></div>' ){
                                            this.loading.toggle     = 'reloading';
                                            document.location.href  = 'admin.php?page=WPSCORE-dashboard';
                                            //self.toggleProduct();
                                        }else{
                                            this.showInstallModal(response.body);
                                        }
                                    }, (response) => {
                                        // error callback
                                    }).then( _ => {
                                        this.loading.install = false;
                                    });

                                },
                                toggleProduct : function(){
                                    this.loading.toggle = true;
                                    this.$http.post(
                                        WPSCORE_dashboard.ajax.url,
                                        {
                                            action              : 'WPSCORE_toggle_' + this.productType,
                                            nonce               : WPSCORE_dashboard.ajax.nonce,
                                            product_folder_slug : this.productFromApi.folder_slug,

                                        },
                                        {
                                            emulateJSON: true
                                        })
                                    .then((response) => {

                                        // success callback
                                        if( lodash.has(this.installedProduct, 'state') ){
                                            this.installedProduct.state = response.body.product_state;
                                        }
                                    }, (response) => {
                                        // error callback
                                    }).then( _ => {
                                        this.loading.toggle     = 'reloading';
                                        if( this.installedProduct.state == 'activated' ){
                                            document.location.href  = 'admin.php?page=WPSCORE-dashboard&activated=true';
                                        }else{
                                            document.location.href  = 'admin.php?page=WPSCORE-dashboard';
                                        }
                                    });
                                },
                                showRequirementsModal: function( productInfos, allRequirementsOk ){
                                    this.$emit('show-requirements-modal', productInfos, allRequirementsOk);
                                },
                                showInstallModal: function(productInfos){
                                    this.$emit('show-install-modal', productInfos);
                                },
                                showConnectionInfosModal: function(){
                                    this.$emit('show-info-modal');
                                }
                            }
                        }
                    }
                },
            }

        });
    }
}