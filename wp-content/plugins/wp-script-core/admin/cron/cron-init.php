<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function WPSCORE_cron_init(){
    WPSCORE()->init( true );
}

if ( !wp_next_scheduled( 'WPSCORE_init') ) {
    wp_schedule_event(  time(), 'twicedaily', 'WPSCORE_init' );
}