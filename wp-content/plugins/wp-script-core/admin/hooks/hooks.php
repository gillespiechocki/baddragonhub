<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_switch_theme(){
    //update site signature
    WPSCORE()->update_client_signature();

    //call init
    WPSCORE()->init( true );
}
add_action('after_switch_theme', 'WPSCORE_switch_theme');

/** Columns admin listing posts */
add_filter('manage_edit-post_columns', 'WPSCORE_add_columns');
function WPSCORE_add_columns($defaults) {
    $defaults['thumb'] = __( 'Thumb', WPSCORE_LANG );
    $defaults['partner'] = __( 'Partner', WPSCORE_LANG );
    return $defaults;
}

add_action('manage_posts_custom_column',  'WPSCORE_columns_content');
function WPSCORE_columns_content($name) {
    global $post;
    $partner = get_post_meta( $post->ID, 'partner', true );
    switch ($name) {
        case 'thumb':
        $attachment = '';
        $attr = '';
        if( isset( $attachment ) && is_object( $attachment ) ){
            $attr = array(
                'alt'   => trim(strip_tags( $attachment->post_excerpt )),
                'title'=> trim(strip_tags( $attachment->post_title ))
                );
        }
        if ( has_post_thumbnail() ) {
            echo get_the_post_thumbnail( $post->ID, 'wpscript_thumb_admin', $attr );
        } else if( get_post_meta($post->ID, 'thumb', true) ){
            echo '<img src="' . get_post_meta($post->ID, 'thumb', true) . '" width="95" height="70" alt="' . get_the_title() . '" />';
        } else {
            echo '<img src="https://res.cloudinary.com/themabiz/image/upload/wpscript/sources/admin-no-image.jpg" width="95" height="70" />';
        }
        break;

        case 'partner':
            echo '<img src="https://res.cloudinary.com/themabiz/image/upload/wpscript/sources/' . $partner . '.jpg" alt="' . $partner . '"/>';
        break;
    }
}
// This theme uses post thumbnails
add_image_size( 'wpscript_thumb_admin', '95', '70', '1' );

function wpscript_admin_notice_updates() {
    $is_core_page = 'toplevel_page_WPSCORE-dashboard' == get_current_screen()->base ? true : false;
    $available_updates = WPSCORE()->get_available_updates();
    if( $available_updates && count($available_updates) > 0 ){
        echo '<div class="notice notice-success is-dismissible">';
                if( $is_core_page ) {
                    echo '<p>Some new WP-Script products versions are available.</p>';
                    echo '<p><i class="fa fa-arrow-down" aria-hidden="true"></i> Just <strong>scroll down on this page and press green update buttons</strong> to update products <i class="fa fa-arrow-down" aria-hidden="true"></i></p>';
                }else{
                    echo '<p>Some new WP-Script products versions are available: </p>';
                    foreach( $available_updates as $update ){
                        if( 'CORE' == $update['product_key'] ){
                            echo "<p>&#10149; {$update['product_title']} <strong>v{$update['product_latest_version']}</strong> &nbsp;&bull;&nbsp; <a href='admin.php?page=WPSCORE-dashboard#wp-script'>Update</a> | <a href='https://www.wp-script.com/core-plugin/' target='_blank'>Changelog</a></p>";
                        }else{
                            echo "<p>&#10149; {$update['product_title']} <strong>v{$update['product_latest_version']}</strong> &nbsp;&bull;&nbsp; <a href='admin.php?page=WPSCORE-dashboard#{$update['product_key']}'>Update</a> | <a href='https://www.wp-script.com/{$update['product_type']}/{$update['product_slug']}/#changelog' target='_blank'>Changelog</a></p>";
                        }
                    }
                }
        echo '</div>';
    }
}
add_action( 'admin_notices', 'wpscript_admin_notice_updates' );