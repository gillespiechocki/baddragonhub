<?php

class WPSCORE_product_uploader {

	private $method, $product;
	
	public function upload_product( $type, $method, $product ){
		$this->product  = $product;
		$this->method		= $method;
		$this->type 		= $type;
		$response				= false;
		
		switch( $type ){
			case 'theme':
				//if user cannot install themes we die
				if ( ! current_user_can('install_themes') )
					return __('You do not have sufficient permissions to install themes for this site.');
				// $response = self::uploadThemeFile();
				$response = self::installTheme();
				break;

			case 'plugin':
				if ( ! current_user_can('install_plugins') )
					return __('You do not have sufficient permissions to install themes for this site.');
				// $response = self::uploadPluginFile();
				$response = self::installPlugin();
				break;

			default:
				break;
		}
		return $response;
	}

	// private function uploadThemeFile() {
	// 	$ftr = $this->product['package'];
	// 	return self::installTheme($ftr);
	// }
	
	// private function uploadPluginFile() {
	// 	$ftr = $this->product['package'];
	// 	return self::installPlugin($ftr);
	// }
	
	private function installTheme() {
	
		require_once(ABSPATH . 'wp-admin/includes/admin.php');
		
		$upgrader = new Theme_Upgrader( new Theme_Quiet_Skin() );

		switch( $this->method ){
			case 'install':
				ob_start();
				$result = $upgrader->install( $this->product['package'] );
				$data 	= ob_get_contents();
				ob_clean();
				break;
			case 'upgrade':
				//self::inject_update_theme_info();
				ob_start();
				$result = $upgrader->upgrade( $this->product['slug'] );
				$data 	= ob_get_contents();
				ob_clean();
				break;
			default:
				return false;
				break;
		}
		self::cleanup($file);
		if(!$results){
			return $data;
		}else{
			return true;
		}
	}
	
	private function installPlugin() {

		require_once(ABSPATH . 'wp-admin/includes/admin.php');
		
		$upgrader = new Plugin_Upgrader( new Plugin_Quiet_Skin() );
		
		switch( $this->method ){
			case 'install':
				ob_start();
				$result = $upgrader->install( $this->product['package'] );
				$data = ob_get_contents();
    		ob_clean();
				break;
			case 'upgrade':
				//self::inject_update_plugin_info();
				ob_start();
				$result = $upgrader->bulk_upgrade( array( $this->product['file_path'] ) );
				$data = ob_get_contents();
    	  ob_clean();
				break;
			default:
				return false;
				break;
		}
		self::cleanup($file);
		if(!$results){
			return $data;
		}else{
			return true;
		}
	}
	
	private function cleanup($file) {
		if(file_exists($file)):
			return @unlink( $file );
		endif;
	}

	private function inject_update_theme_info() {
		$repo_updates = get_site_transient( 'update_themes' );
		if ( ! is_object( $repo_updates ) ) {
			$repo_updates = new stdClass;
		}
		$slug = $this->product['slug'];
		// We only really need to set package, but let's do all we can in case WP changes something.
		$repo_updates->response[ $slug ]['theme']       = $this->product['slug'];
		$repo_updates->response[ $slug ]['new_version'] = $this->product['new_version'];
		$repo_updates->response[ $slug ]['package']     = $this->product['package'];
		$repo_updates->response[ $slug ]['url'] 		= 'https://www.wp-script.com';
		set_site_transient( 'update_themes', $repo_updates );
	}

	private function inject_update_plugin_info() {
		$repo_updates = get_site_transient( 'update_plugins' );
		if ( ! is_object( $repo_updates ) ) {
			$repo_updates = new stdClass;
		}
		$file_path = $this->product['file_path'];
		if ( empty( $repo_updates->response[ $file_path ] ) ) {
			$repo_updates->response[ $file_path ] = new stdClass;
		}
		// We only really need to set package, but let's do all we can in case WP changes something.
		$repo_updates->response[ $file_path ]->slug        	= $this->product['slug'];
		$repo_updates->response[ $file_path ]->plugin      	= $this->product['file_path'];
		$repo_updates->response[ $file_path ]->new_version 	= $this->product['new_version'];
		$repo_updates->response[ $file_path ]->package     	= $this->product['package'];
		$repo_updates->response[ $file_path ]->url 			= 'https://www.wp-script.com';
		set_site_transient( 'update_plugins', $repo_updates );
	}

}

require_once(ABSPATH . 'wp-admin/includes/class-wp-upgrader.php');
class Plugin_Quiet_Skin extends Plugin_Installer_Skin {
	public function feedback($string)
	{
		// just keep it quiet
	}
}

class Theme_Quiet_Skin extends Theme_Installer_Skin {
	public function feedback($string)
	{
		// just keep it quiet
	}
}