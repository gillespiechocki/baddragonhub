<?php

// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

class WPSCORE_admin_menu {
    public function __construct() {
        add_action( 'admin_menu', array( $this, 'register_menus' ) );
    }
    public function register_menus() {
        add_menu_page( __( WPSCORE_NAME, WPSCORE_LANG ), __( WPSCORE_NAME, WPSCORE_LANG ), 'manage_options', 'WPSCORE-dashboard', 'WPSCORE_dashboard_page', WPSCORE_URL . 'admin/assets/images/logo-wp-script-menu.png');
        WPSCORE()->generate_sub_menu();
    }   
}

$WPSCORE_menu = new WPSCORE_admin_menu();