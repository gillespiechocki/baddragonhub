<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_connect_product(){
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        wp_die ( 'Busted!');
    
    if( ! isset( $_POST['product_sku'], $_POST['product_title'] ) )
        wp_die ( 'product_sku needed' );
    
    $api_params = array(
                        'core_version'  => WPSCORE_VERSION,
                        'license_key'   => WPSCORE()->get_license_key(),
                        'product_sku'   => $_POST['product_sku'],
                        'server_addr'   => WPSCORE()->get_server_addr(),
                        'server_name'   => WPSCORE()->get_server_name(),
                        'signature'		=> WPSCORE()->get_client_signature(),
                        'time'			=> ceil( time() / 1000 ) //100
                        );

    $args = array(
                'timeout' 	=> 50,
                'sslverify' => false
            );
            
    $base64_params = base64_encode( serialize( $api_params ) );

    // Send the request
    $response = wp_remote_get( WPSCORE()->get_api_url( 'connect_product', $base64_params), $args );

    if ( !is_wp_error( $response ) && strpos( $response['headers']['content-type'], 'application/json' ) !== false ) {

		$response_body = json_decode( wp_remote_retrieve_body( $response ) );
		
		if( 200 !== $response_body->data->status ){

			WPSCORE()->write_log('error','Connection to API (connect_product) failed (status: <code>' . $response_body->data->status . '</code> message: <code>' . $response_body->message . '</code>)', __FILE__, __LINE__ );
		
        }else{
			if( $response_body->code === 'success' ){
                WPSCORE()->update_product_status( $_POST['product_sku'], $response_body->data->product_status );
				WPSCORE()->write_log( 'success','Product connected <code>'. $_POST['product_title'] . '</code>', __FILE__, __LINE__ );
			}else{
				WPSCORE()->write_log('error','Connection to API (connect_product) failed (status: <code>' . $response_body->data->status . '</code> message: <code>' . $response_body->message . '</code>)', __FILE__, __LINE__ );
			}
		}
		
	}

    wp_send_json( $response_body );

    wp_die();
}
add_action('wp_ajax_WPSCORE_connect_product', 'WPSCORE_connect_product');