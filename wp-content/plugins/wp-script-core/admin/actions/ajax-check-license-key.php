<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_check_license_key(){
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        wp_die ( 'Busted!');

    if( ! isset($_POST['license_key'] ) )
				wp_die ( 'license_key needed' );
				
    $api_params = array(
					'core_version'  => WPSCORE_VERSION,
					'licenseKey'    => $_POST['license_key'],
					'server_addr'   => WPSCORE()->get_server_addr(),
					'server_name'   => WPSCORE()->get_server_name(),
					'signature'			=> WPSCORE()->get_client_signature(),
					'time'					=> ceil( time() / 1000 ) //100
					);

	$args = array(
				'timeout' 	=> 50,
				'sslverify' => false
			);
			
	$base64_params = base64_encode( serialize( $api_params ) );

	// Send the request
	$response = wp_remote_get( WPSCORE()->get_api_url( 'check_license', $base64_params), $args );

	if ( !is_wp_error( $response ) && strpos( $response['headers']['content-type'], 'application/json' ) !== false ) {

		$response_body = json_decode( wp_remote_retrieve_body( $response ) );
		
		if( 200 !== $response_body->data->status ){
			WPSCORE()->write_log('error','Connection to API (check_license) failed (status: <code>' . $response_body->data->status . '</code> message: <code>' . $response_body->message . '</code>)', __FILE__, __LINE__ );
		
		}else{

			if( $response_body->code === 'success' ){
				WPSCORE()->update_license_key( $_POST['license_key'] );
				WPSCORE()->init(true);
				WPSCORE()->write_log( 'success','License Key changed successfully', __FILE__, __LINE__ );
			}else{
				// WPSCORE()->update_license_key( '' );
				WPSCORE()->write_log('error','Connection to API (check_license) failed (status: <code>' . $response_body->data->status . '</code> message: <code>' . $response_body->message . '</code>)', __FILE__, __LINE__ );
			}
		}
		
		wp_send_json( $response_body );
		
	}else{
		WPSCORE()->update_license_key( '' );
		$message = $error = json_encode($response);
		WPSCORE()->write_log('error','Connection to API (check_license) failed (status: <code>' . $error . '</code>)', __FILE__, __LINE__ );
		if( strpos($error, 'cURL error 35') !== false ){
			$message = 'Please update your cUrl version';
		}
		$output = array(
			'code' => 'error',
			'message' => $message
		);
		wp_send_json( $output	);
	}
    wp_die();
}
add_action('wp_ajax_WPSCORE_check_license_key', 'WPSCORE_check_license_key');