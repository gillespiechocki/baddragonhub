<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_toggle_theme(){
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        wp_die ( 'Busted!');
    if( ! isset($_POST['product_folder_slug'] ) )
        wp_die ( 'product_folder_slug is missing!' );

    switch_theme( $_POST['product_folder_slug'] );

    $output['product_state'] = 'activated';

    wp_send_json( $output );

    wp_die();
}
add_action('wp_ajax_WPSCORE_toggle_theme', 'WPSCORE_toggle_theme');