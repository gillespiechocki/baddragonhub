<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_delete_logs(){
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        wp_die ( 'Busted!');

    WPSCORE()->delete_logs();
    
    wp_die();
}
add_action('wp_ajax_WPSCORE_delete_logs', 'WPSCORE_delete_logs');