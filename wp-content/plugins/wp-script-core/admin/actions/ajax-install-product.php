<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_install_product(){
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        wp_die ( 'Busted!');

    if( ! isset( $_POST['method'], $_POST['product_type'], $_POST['product_sku'], $_POST['product_zip'], $_POST['product_slug'], $_POST['product_folder_slug'], $_POST['new_version'] ) )
        wp_die ( 'Some parameters are missing!' );

    $product = array( 
                        'file_path'     => $_POST['product_folder_slug'] . '/' . $_POST['product_folder_slug'] . '.php',
                        'package'       => $_POST['product_zip'],
                        'new_version'   => $_POST['new_version'],
                        'slug'          => $_POST['product_slug'],
                    );
    $installer = new WPSCORE_product_uploader();
    $output = $installer->upload_product( $_POST['product_type'], $_POST['method'], $product );

    //init installed products options
    $options = array(
        'sku'               => $_POST['product_sku'],
        'installed_version' => $_POST['new_version'],
        'state'             => 'deactivated'
    );

    wp_send_json( $output );

    wp_die();
}
add_action('wp_ajax_WPSCORE_install_product', 'WPSCORE_install_product');

