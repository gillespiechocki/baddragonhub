<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_load_dashboard_data(){
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        wp_die ( 'Busted!');

    $current_user = wp_get_current_user();
    
    $data = array(
        'user_email'            => $current_user->user_email,
        'core'                  => WPSCORE()->get_core_options(),
        'installed_products'    => WPSCORE()->get_installed_products(),
        'wps_gold'              => WPSCORE()->get_option('wpsgold'),
        'products'              => WPSCORE()->get_products_options( array( 'data', 'eval' ) ),
        'user_license'          => WPSCORE()->get_license_key()
    );

    wp_send_json( $data );

    wp_die();
}
add_action('wp_ajax_WPSCORE_load_dashboard_data', 'WPSCORE_load_dashboard_data');