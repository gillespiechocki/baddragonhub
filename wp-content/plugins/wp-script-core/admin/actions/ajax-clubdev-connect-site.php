<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_wpsgold_connect_site(){
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        wp_die ( 'Busted!');
        
    $api_params = array(
                        'core_version'  => WPSCORE_VERSION,
                        'license_key'   => WPSCORE()->get_license_key(),
                        'server_addr'   => WPSCORE()->get_server_addr(),
                        'server_name'   => WPSCORE()->get_server_name(),
                        'signature'		=> WPSCORE()->get_client_signature(),
                        'time'			=> ceil( time() / 1000 ) //100
                        );

    $args = array(
                'timeout' 	=> 50,
                'sslverify' => false
            );
            
    $base64_params = base64_encode( serialize( $api_params ) );

    // Send the request
    $response = wp_remote_get( WPSCORE()->get_api_url( 'wpsgold_connect_site', $base64_params), $args );

    if ( !is_wp_error( $response ) && strpos( $response['headers']['content-type'], 'application/json' ) !== false ) {

		$response_body = json_decode( wp_remote_retrieve_body( $response ) );
		
		if( 200 !== $response_body->data->status ){
			WPSCORE()->write_log('error','Connection to API (wpsgold_connect_site) failed (status: <code>' . $response_body->data->status . '</code> message: <code>' . $response_body->message . '</code>)', __FILE__, __LINE__ );
        }else{
			if( $response_body->code === 'success' ){
				WPSCORE()->write_log( 'success','Site connected with WP-Script GOLD</code>', __FILE__, __LINE__ );
			}else{
				WPSCORE()->write_log('error','Connection to API (wpsgold_connect_site) failed (status: <code>' . $response_body->data->status . '</code> message: <code>' . $response_body->message . '</code>)', __FILE__, __LINE__ );
			}
		}
	}

    wp_send_json( $response_body );

    wp_die();
}
add_action('wp_ajax_WPSCORE_wpsgold_connect_site', 'WPSCORE_wpsgold_connect_site');