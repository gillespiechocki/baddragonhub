<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_load_logs_data(){
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        wp_die ( 'Busted!');

    $data = array();
    
    $data['logs'] = WPSCORE()->get_logs();

    wp_send_json( $data );

    wp_die();
}
add_action('wp_ajax_WPSCORE_load_logs_data', 'WPSCORE_load_logs_data');