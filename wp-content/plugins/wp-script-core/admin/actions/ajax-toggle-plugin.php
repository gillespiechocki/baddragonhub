<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function WPSCORE_toggle_plugin(){

    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        wp_die ( 'Busted!');
    if( ! isset($_POST['product_folder_slug'] ) )
        wp_die ( 'product_folder_slug is missing!' );
    
    $plugin_path = $_POST['product_folder_slug'] . '/' . $_POST['product_folder_slug'] . '.php';

    if( ! is_plugin_active( $plugin_path ) ){
        //deactivation
        //null == activation success
        $result = activate_plugin( $plugin_path );
        if ( !is_wp_error( $result ) ) {
            $output['product_state'] = 'activated';
        }else{
            $output['error'] = true;
        }
    }else{
        //activation
        //null == activation success
        $result = deactivate_plugins( $plugin_path );
        if ( !is_wp_error( $result ) ) {
            $output['product_state'] = 'deactivated';
        }else{
            $output['error'] = true;
        }
    }
    
    wp_send_json( $output );

    wp_die();
}
add_action('wp_ajax_WPSCORE_toggle_plugin', 'WPSCORE_toggle_plugin');