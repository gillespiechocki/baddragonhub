<?php
// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

//Define Constants
define( 'WPSCORE_VERSION',    '1.2.5' );                      // Plugin version
define( 'WPSCORE_DIR',        plugin_dir_path( __FILE__ ) );  // Plugin Folder Path
define( 'WPSCORE_URL',        plugin_dir_url( __FILE__ ) );   // Plugin Folder URL
define( 'WPSCORE_FILE',       __FILE__ );                     // Plugin Root File
define( 'WPSCORE_LOG_FILE',   plugin_dir_path( WPSCORE_FILE ) . 'admin/logs/wpscript.log' );// Plugin Log File
define( 'WPSCORE_LANG',       'wp-script' );                  // Plugin lang
define( 'WPSCORE_API_URL',    'https://www.wp-script.com/wp-json/wpsevsl/v2' );// Plugin lang
define( 'WPSCORE_NAME',       'WP-Script' );                  // Plugin lang

//navigation config
self::$config['nav']  = array(
    '0' => array(
        'slug' 		=> 'WPSCORE-dashboard',
        'callback' 	=> 'WPSCORE_dashboard_page',
        'title'		=> 'Dashboard',
        'icon'		=> 'fa-tachometer',
    ),
    '1000' => array(
        'slug' 		=> 'WPSCORE-logs',
        'callback' 	=> 'WPSCORE_logs_page',
        'title'		=> 'Logs',
        'icon'		=> 'fa-file-text-o',
    )
);

//js config
self::$config['scripts']['js'] = array(
    //vendor
    'WPSCORE_lodash.js'             => array(
                                        'in_pages'  => 'wpscript_pages',
                                        'path'      => 'admin/vendors/lodash/lodash.min.js',
                                        'require'   => array(),
                                        'version'   => '4.17.4',
                                        'in_footer' => false,
    ),
    'WPSCORE_bootstrap.js'          => array(
                                        'in_pages'  => 'wpscript_pages',
                                        'path'      => 'admin/vendors/bootstrap/js/bootstrap.min.js',
                                        'require'   => array('jquery'),
                                        'version'   => '3.2.0',
                                        'in_footer' => false,
    ),
    'WPSCORE_vue.js'                => array(
                                        'in_pages'  => 'wpscript_pages',
                                        'path'      => 'admin/vendors/vue/vue.js',
                                        'require'   => array(),
                                        'version'   => '2.4.0',
                                        'in_footer' => false,
    ),
    'WPSCORE_vue-resource.js'       => array(
                                        'in_pages'  => 'wpscript_pages',
                                        'path'      => 'admin/vendors/vue-resource/vue-resource.min.js',
                                        'require'   => array(),
                                        'version'   => '1.0.3',
                                        'in_footer' => false,
    ),
    'WPSCORE_clipboard.js'          => array(
                                        'in_pages'  => array('WPSCORE-logs'),
                                        'path'      => 'admin/vendors/clipboard/clipboard.min.js',
                                        'require'   => array(),
                                        'version'   => '1.6.0',
                                        'in_footer' => false,
    ),
    //pages
    'WPSCORE_dashboard.js'          => array(
                                        'in_pages'  => array('WPSCORE-dashboard'),
                                        'path'      => 'admin/pages/page-dashboard.js',
                                        'require'   => array(),
                                        'version'   => WPSCORE_VERSION,
                                        'in_footer' => false,
                                        'localize'  => array(
                                                            'ajax'      => true,
                                                            'objectL10n'=> array(
                                                                                //'sentence_id'     => __( 'Sentence', 'WPSCORE_LANG' ),
                                                            )
                                        )
    ),
    'WPSCORE_logs.js'               => array(
                                        'in_pages'  => array('WPSCORE-logs'),
                                        'path'      => 'admin/pages/page-logs.js',
                                        'require'   => array(),
                                        'version'   => WPSCORE_VERSION,
                                        'in_footer' => false,
                                        'localize'  => array(
                                                            'ajax'      => true,
                                                            'objectL10n'=> array(
                                                                                //'sentence_id'     => __( 'Sentence', 'WPSCORE_LANG' ),
                                                            )
                                        )
    )
);

//css config
self::$config['scripts']['css'] = array(
    //vendor
    'WPSCORE_fontawesome.css'          => array(
                                            'in_pages'  => 'wpscript_pages',
                                            'path'      => 'admin/vendors/font-awesome/css/font-awesome.min.css',
                                            'require'   => array(),
                                            'version'   => '4.6.0',
                                            'media'     => 'all'
    ),
    'WPSCORE_bootstrap.css'          => array(
                                            'in_pages'  => 'wpscript_pages',
                                            'path'      => 'admin/vendors/bootstrap/css/bootstrap.min.css',
                                            'require'   => array(),
                                            'version'   => '3.2.0',
                                            'media'     => 'all'
    ),
    //assets
    'WPSCORE_admin.css'          => array(
                                            'in_pages'  => 'wpscript_pages',
                                            'path'      => 'admin/assets/css/admin.css',
                                            'require'   => array(),
                                            'version'   => WPSCORE_VERSION,
                                            'media'     => 'all'
    )
);