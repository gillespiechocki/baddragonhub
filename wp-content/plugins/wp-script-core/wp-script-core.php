<?php
/**
* Plugin Name: WP-Script Core
* Plugin URI: https://www.wp-script.com
* Description: WP-Script.com core plugin
* Author: WP-Script
* Author URI: https://www.wp-script.com
* Version: 1.2.5
* Text Domain: wp-script-core
* Domain Path: /languages
*/

// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

//Singleton Class
final class WPSCORE {

        /****************/
      /*** INSTANCE ***/
    /****************/

    private static $instance, $config;

    public function __clone()   { _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'WPSCORE' ), '1.0' ); }
    public function __wakeup()  { _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'WPSCORE' ), '1.0' ); }

    public static function instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof WPSCORE ) ) {
            self::$instance = new WPSCORE;

            //load config
            require_once( plugin_dir_path( __FILE__ ) . 'config.php' );

            //load options system
            if( WPSCORE()->php_version_ok() )
                require_once( plugin_dir_path( __FILE__ ) . 'xbox/xbox.php' );

            if( is_admin() ) {

                //load hooks
                self::$instance->load_hooks();
                
                //auto-load admin php files
                self::$instance->auto_load_php_files( 'admin' );
                self::$instance->init( false );
                self::$instance->load_textdomain();
            }else{
                //auto-load public php files
                //self::$instance->auto_load_php_files( 'public' );
            }
        }
        return self::$instance;
    }

    public function load_hooks() {

        //save default options for themes
        add_action('admin_init', array( $this, 'save_default_options') );

        //load js and css files for all products
        add_action('wp_print_scripts', array( $this, 'auto_load_scripts'), 100 );

        //reorder submenu
        add_action( 'admin_init', array( $this, 'reorder_submenu' ) );

        //plugin activation / deactivation / uninstall
        register_activation_hook( __FILE__, array( $this, 'activation' ) );
        register_deactivation_hook( __FILE__, array( $this, 'deactivation' ) );
        //register_uninstall_hook( __FILE__, array( $this, 'uninstall' ) );
    }

    public function save_default_options(){
        if( !isset( $_GET['page'] ) || 'WPSCORE-dashboard' != $_GET['page'] || !WPSCORE()->php_version_ok() ) return;

        $all_options = xbox_get_all();
        foreach( (array) $all_options as $xbox_id => $xbox_options ){
            if( get_option( $xbox_id ) === false ){
                $xbox = xbox_get( strtolower( $xbox_id ) );
                if( $xbox ){
                    $xbox->save_fields(0, array( 'display_message_on_save' => false ));
                }
            }
        }
    }

    public function auto_load_scripts() {

        $scripts = apply_filters( 'WPSCORE-scripts', self::$config['scripts']['js'] + self::$config['scripts']['css'] );
        $wpscript_pages = $this->get_pages_slugs();

        //remove others scripts and css on WP-Script pages excepted options pages
        /*if( isset( $_GET['page'] ) && in_array( $_GET['page'], $wpscript_pages ) && strpos( $_GET['page'], '-options' ) === false ){

            global $wp_scripts, $wp_styles;
            // reset all styles
            $wp_styles = new WP_Styles();

            // reset all scripts
            $wp_scripts = new WP_Scripts();

        }*/

        //add wp-script scripts and css on WP-Script pages
        foreach( (array) $scripts as $k => $v ) {
            if( !isset( $v['in_pages'] ) || isset( $_GET['page'] ) && in_array( $_GET['page'], $v['in_pages'] == 'wpscript_pages' ? $wpscript_pages : $v['in_pages'] ) ) {
                $type = explode( '.', $k ); $type = end( $type );
                $sku  = explode( '_', $k ); $sku  = current( $sku );
                $path = str_replace( array('http:', 'https:'), array('', ''), constant( $sku . '_URL' ) . $v['path'] );
                switch( $type ) {
                    case 'js':
                        // exclude script if option pages and script is bootstrap to avoid dropdown conflicts
                        if( strpos( $_GET['page'], '-options' ) !== false && $k === 'WPSCORE_bootstrap.js' ){
                            break;
                        }
                        wp_enqueue_script( $k, $path, $v['require'], $v['version'], $v['in_footer'] );
                        if( isset( $v['localize'] ) && ! empty( $v['localize'] ) ) {
                            if( isset($v['localize']['ajax']) && $v['localize']['ajax'] === true ){
                                $v['localize']['ajax'] = array( 
                                    'url' => str_replace( array('http:', 'https:'), array('', ''), admin_url('admin-ajax.php') ),
                                    'nonce' => wp_create_nonce( 'ajax-nonce' )
                                );
                            }
                            wp_localize_script( $k, str_replace( array('-', '.js'), array('_', ''), $k ), $v['localize'] );
                        }
                        break;
                    case 'css':
                        wp_enqueue_style( $k, $path, $v['require'], $v['version'], $v['media'] );
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public function auto_load_php_files( $dir ) {

        $dirs = apply_filters( 'WPSCORE-' . $dir . '_dirs', (array) ( plugin_dir_path( __FILE__ ) . $dir . '/' ) );

        foreach( (array) $dirs as $dir ){
            
            $files = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $dir ) );
        
            if( ! empty( $files ) ) {
                // /!\ files is an object
                foreach ( $files as $file ) {

                    if ( $file->isDir() || $file->getPathname() == 'index.php' ) { continue; }
                    if( substr($file->getPathname(), -4 ) == '.php' ) { 
                        require $file->getPathname(); }
                }
            }
        }
    }

    static function activation(){
        //update site signature
        WPSCORE()->update_client_signature();
        
        //add cron job
        add_action( 'WPSCORE_init', 'WPSCORE_cron_init');

        //call init
        WPSCORE()->init( true );
    }

    static function deactivation(){
        //update site signature
        WPSCORE()->update_client_signature();

        //remove cron job
        wp_clear_scheduled_hook( 'WPSCORE_init' );

        //call init
        WPSCORE()->init( true );
    }

    static function uninstall(){
        //delete plugin options
        delete_option( 'WPSCORE_options' );

        //remove cron job
        wp_clear_scheduled_hook( 'WPSCORE_init' );

        //call init
        WPSCORE()->init( true );
    }

    public function get_server_addr() {
        return isset($_SERVER["SERVER_ADDR"]) ? $_SERVER["SERVER_ADDR"] : $_SERVER["LOCAL_ADDR"];
    }
    public function get_server_name() {
        return ( $_SERVER["SERVER_NAME"] != '' && $_SERVER["SERVER_NAME"] != '_' ) ? $_SERVER["SERVER_NAME"] : str_replace(array('http://', 'https://'), array('', ''), get_site_url());
    }
    
        /***************/
      /*** options ***/
    /***************/

    //License options methods shortcuts
    public function get_license_key() {
        return $this->get_option( 'wps_license' );
    }

    public function update_license_key( $new_license_key ) {
        return $this->update_option( 'wps_license', $new_license_key );
    }

    //Signature options methods shortcuts
    public function get_client_signature() {
        return $this->get_option( 'signature' );
    }

    public function update_client_signature() {
        if( !$this->get_client_signature() ) return;

        $signature          = $this->get_client_signature();
        $signature->site    = time();
        return $this->update_option( 'signature', $signature );
    }

    //API url
    public function get_api_url( $action, $base64_params ) {
        return WPSCORE_API_URL . '/' . $action . '/' . $base64_params;
    }

    //WPSCORE options methods (shortcuts)
    public function get_options(){
        return $this->get_product_options( 'WPSCORE' );
    }

    public function get_option( $option_key ){
        return $this->get_product_option( 'WPSCORE', $option_key );
    }

    public function update_option( $option_key, $new_value ){
        return $this->update_product_option( 'WPSCORE', $option_key, $new_value );
    }

    public function delete_option( $option_key ){
        return $this->delete_product_option( 'WPSCORE', $option_key );
    }

    //Generic options methods
    public function get_products_options( $options_to_remove = null ){
        $products_options = WPSCORE()->get_option('products');
        if( $products_options == '' ) return;

        if( $options_to_remove === null ) return $products_options;

        $options_to_remove = (array) $options_to_remove;

        foreach( (array) $products_options as $products_type => $products ){
            foreach( (array) $products as $product_sku => $product ){
                foreach( $product as $option_key => $option_value ){
                    if( in_array( $option_key, $options_to_remove ) ){
                        unset( $products_options->$products_type->$product_sku->$option_key );
                    }
                    if( 'requirements' == $option_key){
                        foreach( (array) $option_value as $index => $requirement ){
                            $products_options->$products_type->$product_sku->{$option_key}[$index]->status = $this->check_requirement( $requirement->type, $requirement->name );
                        }
                    }
                }
            }
        }
        if( isset( $products_options->plugins->CORE ) )
            unset( $products_options->plugins->CORE );

        return $products_options;
    }

    public function get_product_options( $product_sku ) {
        $plugin_options = get_option( $product_sku . '_options' );
        return $plugin_options;
    }

    public function get_product_option( $product_sku, $option_key ) {
        if( empty($option_key) )
            return false;
        $product_options = get_option( $product_sku . '_options' );
        return isset( $product_options[$option_key] ) ? $product_options[$option_key] : '';
    }
        
    public function update_product_option( $product_sku, $option_key, $new_value ) {
        if( empty( $option_key ) )
            return false;
        $product_options = get_option( $product_sku . '_options' );
        $product_options[$option_key] = $new_value;
        return update_option( $product_sku . '_options', $product_options );
    }

    public function delete_product_option( $product_sku, $option_key ) {
        if( empty( $option_key ) )
            return false;
        $product_options = get_option( $product_sku . '_options' );
        unset( $product_options[$option_key] );
        return update_option( $product_sku . '_options', $product_options );
    }

    //Products object to array helper
    public function get_products_as_array(){
        $products = json_decode( json_encode( $this->get_option( 'products' ) ), true );
        $merged_products = array();
        if( !$products ) return false;

        foreach( (array) array_keys( $products ) as $product_type ){
            $merged_products = array_merge( (array) $merged_products, (array) $products[$product_type] );
        }
        unset( $products );
        return $merged_products;
    }

    //Product eval method
    public function eval_product_data( $product_sku, $eval_key, $params = null ) {
        $products = $this->get_products_as_array();
        if( empty( $products[$product_sku] ) || empty( $products[$product_sku]['eval'][$eval_key] ) )
            return false;
        return base64_decode( $products[$product_sku]['eval'][$eval_key] );
    }

    //Core methods
    public function get_core_options(){
        $products = $this->get_products_as_array();
        if( ! isset( $products['CORE'] ) )
            return false;
        $products['CORE']['installed_version'] = WPSCORE_VERSION;
        $products['CORE']['is_latest_version'] = version_compare( $products['CORE']['installed_version'], $products['CORE']['latest_version'], '==' );
        return $products['CORE'];
    }

    public function get_core_option( $option_key ){
        $core = $this->get_core_options();
        if( ! isset( $core[$option_key] ) )
            return false;
        return $core[$option_key];
    }

    //Product status methods (generic)
    public function get_product_status( $product_sku ) {
        $products = $this->get_products_as_array();
        if( ! isset( $products[$product_sku]['status'] ) )
            return false;
        return $products[$product_sku]['status'];
    }

    public function update_product_status( $product_sku, $new_status ){
        $products = $this->get_option( 'products' );
        $products->$product_sku->status = $new_status;
        return $this->update_option( 'products', $products );
    }

    public function delete_product_data( $product_type, $product_sku ){
        $products = $this->get_option( 'products' );
        if( isset( $products->$product_type->$product_sku->data ) )
            unset( $products->$product_type->$product_sku->data );
        return $this->update_option( 'products', $products );
    }

    public function delete_product_eval( $product_type, $product_sku ){
        $products = $this->get_option( 'products' );
        if( isset( $products->$product_type->$product_sku->eval ) )
            unset( $products->$product_type->$product_sku->eval );
        return $this->update_option( 'products', $products );
    }

    //Product methods (generic)
    public function get_product_model( $product_sku ) {
        $products = $this->get_products_as_array();
        if( ! isset( $products[$product_sku]['model'] ) )
            return false;
        return $products[$product_sku]['model'];
    }

    public function get_product_data( $product_sku ){
        $products = $this->get_products_as_array();
        if( ! isset( $products[$product_sku]['data'] ) )
            return false;
        return $products[$product_sku]['data'];
    }

        /********************/
      /*** requirements ***/
    /********************/
    public function all_core_requirements_ok(){
        return WPSCORE()->curl_ok() && WPSCORE()->curl_version_ok();
    }

    public function php_version_ok(){
        return version_compare(PHP_VERSION, '5.3.0') >= 0;
    }

    public function curl_ok(){
        return function_exists('curl_version');
    }

    public function get_curl_version(){
        $curl_infos = curl_version();
        return $curl_infos['version'];
    }

    public function curl_version_ok(){
        return version_compare(WPSCORE()->get_curl_version(), '7.34.0') >= 0;
    }

    public function check_requirement( $type, $name ){
        switch( $type ){
            case 'extension':
                return extension_loaded( $name );
                break;
            case 'class':
                return class_exists( $name );
                break;
            case 'function':
                return function_exists( $name );
                break;
            case 'ini':
                return ini_get( $name ) == true;
                break;
        }
    }
    
        /************/
      /*** logs ***/
    /************/

    public function write_log( $type, $entry, $file_uri = '', $file_line = '' ) {
        //genereting $file_short_uri
        $wp_content_index = strpos( $file_uri, 'wp-content' );
        if( $wp_content_index !== false ) {
            $file_uri = '..' . substr( $file_uri, $wp_content_index - 1 );
        }

        $handle = fopen( WPSCORE_LOG_FILE, "a" );
        if( $handle ) {
            $write = fputs( $handle, '[' . current_time('Y-m-d H:i:s') . '][' . $type . '][' . $file_uri . '][' . $file_line . '] ' . $entry . "***" );
        }
        fclose( $handle );
    }

    public function get_logs() {

        $handle = fopen( WPSCORE_LOG_FILE, "r" );
        if (!$handle) {
            return array();
        }

        $lines = explode( '***', file_get_contents(WPSCORE_LOG_FILE) );

        if( count($lines) <= 0 ) {
            return array();
        }

        $output_logs = array();

        foreach( (array)$lines as $line ) {

            if( !empty($line) ){
            
                preg_match_all( "/\[([^\]]*)\]/", $line, $line_data );

                $date       = $line_data[1][0];
                $type       = $line_data[1][1];
                $file_uri   = $line_data[1][2];
                $file_line  = $line_data[1][3];

                switch( $type ) {
                    case 'notice':
                        $class = 'label-info';
                        break;
                    case 'error':
                        $class = 'label-danger';
                        break;
                    default:
                        $class = 'label-' . $type;
                        break;
                }

                $output_logs[] = array(
                    'class'     => $class,
                    'date'      => $date,
                    'file_uri'  => $file_uri,
                    'file_line' => $file_line,
                    'message'   => str_replace( $line_data[0][0] . $line_data[0][1] . $line_data[0][2] . $line_data[0][3] . ' ', '', $line),
                    'type'      => $type
                );
            }
        }

        return $output_logs;
    }

    public function delete_logs() {
        $handle = fopen( WPSCORE_LOG_FILE , "r+" );
        if ( $handle !== false ) {
            ftruncate( $handle, 0 );
            fclose( $handle );
        }
    }


        /********************/
      /*** pages & tabs ***/
    /********************/

    public function get_pages_slugs() {
        $pages = apply_filters( 'WPSCORE-pages', self::$config['nav'] );
        foreach( (array) $pages as $k => $v ) {
            $output[] = $v['slug'];
        }
        //add themes options page
        $output[] = 'wpst-options';
        return $output;
    }

    public function generate_sub_menu() {
        $data = apply_filters( 'WPSCORE-pages', self::$config['nav']);
        ksort($data);
        foreach ( (array) $data as $page) {
            $slug = strtoupper( current( explode( '-', $page['slug'] ) ) );
            if( $slug == 'WPSCORE' || ( WPSCORE()->php_version_ok() && ( $slug == 'WPST' || WPSCORE()->get_product_status( $slug ) == 'connected' ) ) ){
                if( isset( $page['slug'], $page['callback'], $page['title'] ) ){
                    add_submenu_page( 'WPSCORE-dashboard', __( $page['title'], WPSCORE_LANG ), __( $page['title'], WPSCORE_LANG ), 'manage_options', $page['slug'], $page['callback'] );
                }
            }
        }

        add_submenu_page( 'WPSCORE-dashboard', __( 'Help', WPSCORE_LANG ), __( 'Help', WPSCORE_LANG ), 'manage_options', 'https://www.wp-script.com/documentation/');
    }
    
    public function reorder_submenu( $menu_ord ) 
    {
        global $submenu;

        $final_menu = $submenu;

        foreach( (array) $submenu['WPSCORE-dashboard'] as $key => $menu ){
            unset( $final_menu['WPSCORE-dashboard'][$key] );
            if( $menu[2] == 'wpst-options' ){
                $final_menu['WPSCORE-dashboard'][1] = $menu;
            }else{
                $final_menu['WPSCORE-dashboard'][$key * 10] = $menu;
            }
        }
        
        if( is_array( $final_menu['WPSCORE-dashboard'] ) ){
            ksort( $final_menu['WPSCORE-dashboard'], SORT_NUMERIC );
        }

        $submenu = $final_menu;
    }

    public function display_tabs( $echo = true ) {
        $data = apply_filters( 'WPSCORE-tabs', self::$config['nav']);
        ksort($data);
        $buffered_tabs = array();
        $static_tabs_slugs = array('WPSCORE-dashboard', 'wpst-options', 'WPSCORE-logs');

        $output_tabs = '<ul class="nav nav-tabs">';
        // buffer loop
        foreach ( (array) $data as $index => $tab) {
            $slug = strtoupper( current( explode( '-', $tab['slug'] ) ) );
            if( $slug == 'WPSCORE' || ( WPSCORE()->php_version_ok() && ( $slug == 'WPST' || WPSCORE()->get_product_status( $slug ) == 'connected' ) ) ){
                
                if( isset( $tab['slug'], $tab['icon'], $tab['title'] ) ){
                    if( $slug == 'WPSCORE' ){
                        $active = $_GET['page'] == $tab['slug'] ? 'active' : null;
                    }else{
                        $active = strpos( strtolower ( $_GET['page'] ), strtolower( $slug ) ) !== false ? 'active' : null;
                    }
                    if( in_array($tab['slug'], $static_tabs_slugs) ){
                        // buffer statics tabs
                        $buffered_tabs[$index] = '<li class="' . $active . '"><a href="admin.php?page=' .  $tab['slug'] . '"><i class="fa-fw fa ' . $tab['icon'] . '"></i> ' . $tab['title'] . '</a></li>';
                    }else{
                        // buffer plugins sub tabs on tab with index 10 - between theme options (index 1) and logs (index 1000)
                        $buffered_tabs[10][$index] = '<li class="' . $active . '"><a href="admin.php?page=' .  $tab['slug'] . '"><i class="fa-fw fa ' . $tab['icon'] . '"></i> ' . $tab['title'] . '</a></li>';;
                    }
                }
            }
        }
        // Output loop
        foreach ( (array) $buffered_tabs as $index => $tab) {
            if( $index === 10  ){// plugins case
                $inline_plugins_tabs = implode('', $tab);
                $is_active = strpos($inline_plugins_tabs, '<li class="active">') !== false ? 'active' : '';
                $plugin_besides = '';
                if( $is_active ){
                    // retrieve active plugin name
                    $regex = '/<li class="active">.+(<i.+<\/i>)\s(.+)<\/a><\/li>/U';
                    preg_match_all($regex, $inline_plugins_tabs, $matches, PREG_SET_ORDER, 0);
                    $active_plugin_icon = $matches[0][1];
                    $active_plugin_name = $matches[0][2];
                    $plugin_besides = ' <span class="fa fa-caret-right plugins-separator" aria-hidden="true"></span> ' . $active_plugin_icon . ' ' . $active_plugin_name;
                }else{
                    $plugin_besides = ' (' . count($tab) . ')';
                }
                $output_tabs .= '<li class="dropdown ' . $is_active . '">';
                $output_tabs .=     '<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-cogs" aria-hidden="true"></i> ' . __('Plugins', WPSCORE_LANG) . $plugin_besides . ' <span class="caret"></span></a>';
                $output_tabs .=     '<ul class="dropdown-menu">';
                $output_tabs .=         $inline_plugins_tabs;
                $output_tabs .=     '</ul>';
                $output_tabs .= '</li>';
            }else{
                $output_tabs .= $tab;
            }
        }

        $output_tabs .= '<li><a href="https://www.wp-script.com/documentation/" target="_blank"><i class="fa fa-life-ring"></i> ' .  __('Help', WPSCORE_LANG) . '</a></li>';

        $output_tabs .= '</ul>';

        if( $echo ){
            echo $output_tabs;
        }
        else{
            return $output_tabs;
        }
    }

    public function get_installed_theme( $option_key = null ) {
        $installed_products = $this->get_installed_products();
        if( ! isset( $installed_products['themes'] ) )
            return false;
        $installed_theme = current( $installed_products['themes'] );
        if( $option_key != null && isset( $installed_theme[$option_key] ) ){
            return $installed_theme[$option_key];
        }
        return $installed_theme;
    }

    public function init_installed_products() {

        $products_from_api = $this->get_option('products');
        // return false to prevent warning on first load
        if( !$products_from_api ) return false;

        if ( ! function_exists( 'get_plugins' ) ) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }

        $active_theme = wp_get_theme();
        $installed_products = array();

        foreach ( (array) $products_from_api as $type => $products) {

            $installed_products[$type] = array();

            foreach( (array) $products as $product ) {

                switch( $type ) {
                    case 'themes':
                        if( isset($product->folder_slug) ){
                            
                            $theme  = wp_get_theme($product->folder_slug);

                            if ( $theme->exists() ){
                                $installed_products[$type][$product->sku]['sku'] = $product->sku;
                                $installed_products[$type][$product->sku]['installed_version'] = $theme->get( 'Version' );
                                
                                if( $active_theme->get( 'Name' ) == $theme->get( 'Name' ) ){
                                    $installed_products[$type][$product->sku]['state'] = 'activated';
                                }else{
                                    $installed_products[$type][$product->sku]['state'] = 'deactivated';
                                }
                            }
                        }
                        break;

                    case 'plugins':

                        $plugins = get_plugins();
                        $plugin_path = $product->folder_slug . '/' . $product->folder_slug . '.php';

                        if( isset( $plugins[$plugin_path] ) && is_array( $plugins[$plugin_path] ) ) {
                            
                            $installed_products[$type][$product->sku]['sku'] = $product->sku;
                            $installed_products[$type][$product->sku]['installed_version'] = $plugins[$plugin_path]['Version'];
                            
                            if( is_plugin_active( $plugin_path ) ){
                                $installed_products[$type][$product->sku]['state'] = 'activated';
                            }else{
                                $installed_products[$type][$product->sku]['state'] = 'deactivated';
                            }
                        }
                        break;
                    
                    default:
                        break;
                }
            }

        }
        $installed_products = array_reverse( (array) $installed_products );

        WPSCORE()->update_option( 'installed_products', $installed_products );
        
        return $installed_products;
    }

    function get_installed_products(){
        return WPSCORE()->get_option('installed_products');
    }
    
    function get_available_updates(){
        $installed_products = WPSCORE()->get_installed_products();
        $products_from_api  = WPSCORE()->get_products_options( array( 'data', 'eval' ) );
        $core_data          = WPSCORE()->get_core_options();
        $available_updates  = array();

        if( !$installed_products || !$products_from_api || !$core_data ){
            return false;
        }
        // TODO : corriger quand $installed_products est invalid (vérifier quel type de donnée est renvoyé par $installed_products)
        foreach( $installed_products as $products_type => $products_data ){
            foreach( $products_data as $product_key => $product_data ){

                // exclude deconnected products from updates
                if( $product_key != 'CORE' && $products_from_api->$products_type->$product_key->status != 'connected' ) continue;

                
                if( $product_key == 'CORE' ){
                    if( ! $core_data['is_latest_version'] ){
                        $available_updates[] = array( 
                            'product_key'           => $product_key,
                            'product_title'         => $core_data['title'],
                            'product_latest_version'=> $core_data['latest_version']
                        );
                    }
                }else{
                    if( $product_data['installed_version'] !== $products_from_api->$products_type->$product_key->latest_version ){
                        $available_updates[] = array( 
                            'product_key'           => $product_key,
                            'product_title'         => $products_from_api->$products_type->$product_key->title,
                            'product_latest_version'=> $products_from_api->$products_type->$product_key->latest_version,
                            'product_slug'          => $products_from_api->$products_type->$product_key->slug,
                            'product_type'          => $products_type
                        );
                    }
                }
            }
        }
       
        
        return $available_updates;
    }


    /**
    * Do init actions
    *
    * @access private
    * @since 1.0
    * @return void
    */
    public function init( $force = false ) {

        if( isset( $_GET['page'] ) && $_GET['page'] == 'WPSCORE-dashboard' || $force ) {

            if( !$this->get_license_key() ) {
                return false;
            }

            $current_theme = wp_get_theme();

            $api_params = array(
                'license_key'   => $this->get_license_key(),
                'signature'     => $this->get_client_signature(),
                'server_addr'   => $this->get_server_addr(),
                'server_name'   => $this->get_server_name(),
                'core_version'  => WPSCORE_VERSION,
                'time'          => ceil( time() / 1000 ),
                'current_theme' => array(
                                        'name'              => $current_theme->get( 'Name' ),
                                        'version'           => $current_theme->get( 'Version' ),
                                        'theme_uri'         => $current_theme->get( 'ThemeURI' ),
                                        'template'          => $current_theme->get( 'Template' ),
                                        ),
                //TODO Get Child Theme infos
                'products'      => $this->init_installed_products()
            );

            $args = array(
                        'timeout'   => 50,
                        'sslverify' => true,
            );

            $base64_params = base64_encode( serialize($api_params) );

            $response = wp_remote_get( $this->get_api_url( 'init', $base64_params), $args );

            if ( !is_wp_error( $response ) && $response['headers']['content-type'] === 'application/json; charset=UTF-8' ) {

                $response_body = json_decode( wp_remote_retrieve_body( $response ) );


                // echo '<xmp>';
                // var_dump($response_body);
                // echo '</xmp>';

                if( $response_body === null ){
                    $this->write_log( 'error','Connection to API (init) failed (null)', WPSCORE_FILE, __LINE__ );
                }elseif( 200 !== $response_body->data->status ) {
                    $this->write_log( 'error','Connection to API (init) failed (status: <code>' . $response_body->data->status . '</code> message: <code>' . $response_body->message . '</code>)', WPSCORE_FILE, __LINE__ );
                }else{
                    if( isset($response_body->code) && $response_body->code == 'error' ){
                        $this->write_log( 'error','Connection to API (init) failed <code>' . $response_body->message . '</code>', WPSCORE_FILE, __LINE__ );
                    }else{
                        if( isset($response_body->data->signature) ) {
                            $this->update_option( 'signature', $response_body->data->signature );
                        }
                        if( isset($response_body->data->products) ) {
                            $this->update_option( 'products', $response_body->data->products );
                        }
                        if( isset($response_body->data->wpsgold) ) {
                            $this->update_option( 'wpsgold', $response_body->data->wpsgold );
                        }
                        //products updates
                        $repo_updates_themes    = get_site_transient( 'update_themes' );
                        $repo_updates_plugins   = get_site_transient( 'update_plugins' );

                        $installed_products = $this->get_installed_products();

                        foreach( (array) $installed_products as $installed_product_type => $installed_products ){
                            foreach( (array) $installed_products as $installed_product_sku => $installed_product_infos ){

                                $product = $response_body->data->products->$installed_product_type->$installed_product_sku;

                                if( version_compare( $installed_product_infos['installed_version'], $product->latest_version ) !== 0 ){
                                    if( $installed_product_type == 'themes' ){
                                        //theme update found
                                        if ( ! is_object( $repo_updates_themes ) ) {
                                            $repo_updates_themes = new stdClass;
                                        }
                                        $slug = $product->slug;
                                        $repo_updates_themes->response[ $slug ]['theme']       = $product->slug;
                                        $repo_updates_themes->response[ $slug ]['new_version'] = $product->latest_version;
                                        $repo_updates_themes->response[ $slug ]['package']     = $product->zip_file;
                                        $repo_updates_themes->response[ $slug ]['url']         = 'https://www.wp-script.com';
                                        set_site_transient( 'update_themes', $repo_updates_themes );
                                    }else{
                                        //plugin update found
                                        if ( ! is_object( $repo_updates_plugins ) ) {
                                            $repo_updates_plugins = new stdClass;
                                        }
                                        $file_path = $product->slug . '/' . $product->slug . '.php';
                                        
                                        if ( empty( $repo_updates_plugins->response[ $file_path ] ) ) {
                                            $repo_updates_plugins->response[ $file_path ] = new stdClass;
                                        }

                                        $repo_updates_plugins->response[ $file_path ]->slug         = $product->slug;
                                        $repo_updates_plugins->response[ $file_path ]->new_version  = $product->latest_version;
                                        $repo_updates_plugins->response[ $file_path ]->author       = 'WP-Script';
                                        $repo_updates_plugins->response[ $file_path ]->homepage     = 'https://www.wp-script.com';
                                        $repo_updates_plugins->response[ $file_path ]->package      = $product->zip_file;
                                        
                                        set_site_transient( 'update_plugins', $repo_updates_plugins );
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                if( isset( $response->errors['http_request_failed'] ) ) {
                    $this->write_log( 'error','Connection to API (init) failed <code>' . $response->errors . '</code>', WPSCORE_FILE, __LINE__ );
                }
            }
        }
    }


        /*******************/
      /*** TEXT DOMAIN ***/
    /*******************/
    private function load_textdomain() {
        // Set filter for plugin's languages directory
        $lang_dir = dirname( plugin_basename( WPSCORE_FILE ) ) . '/languages/';

        // Traditional WordPress plugin locale filter
        $mofile = sprintf( '%1$s-%2$s.mo', WPSCORE_LANG, get_locale() );

        // Setup paths to current locale file
        $mofile_local  = $lang_dir . $mofile;
        $mofile_global = WP_LANG_DIR . '/' . WPSCORE_LANG . '/' . $mofile;

        if ( file_exists( $mofile_global ) ) {
            // Look in global /wp-content/languages/WPSCORE/ folder
            load_textdomain( WPSCORE_LANG, $mofile_global );
        } elseif ( file_exists( $mofile_local ) ) {
            // Look in local /wp-content/plugins/WPSCORE/languages/ folder
            load_textdomain( WPSCORE_LANG, $mofile_local );
        } else {
            // Load the default language files
            load_plugin_textdomain( WPSCORE_LANG, false, $lang_dir );
        }
        return false;
    }

}

//fix xbox functions if php < 5.3
if( !WPSCORE()->php_version_ok() ){

    if( !function_exists( 'xbox_get_all' ) ){
        function xbox_get_all(){
            return false;
        }
    }
    
    if( !function_exists( 'xbox_get' ) ){
        function xbox_get( $xbox_id ){
            return false;
        }
    }

    if( !function_exists( 'xbox_new_metabox' ) ){
        function xbox_new_metabox( $options = array() ){
            return false;
        }
    }

    if( !function_exists( 'xbox_new_admin_page' ) ){
        function xbox_new_admin_page( $options = array() ){
            return false;
        }
    }

    if( !function_exists( 'xbox_get_field_value' ) ){
        function xbox_get_field_value( $xbox_id, $field_id = '', $default = '', $post_id = '' ){
            return false;
        }
    }
}

function WPSCORE() {
    return WPSCORE::instance();
}
WPSCORE();